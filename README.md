# DISSOTTI - SOFTWARE LIVRE #
O projeto Dissotti Software Livre é uma inciativa dos desenvolvedores Marcos e Gabriel, onde o uníco objetivo é ajudar a todos crescerem com nossa equipe!
 
 - Acesse o link do website: http://dissotti.com.br
 
 - Para contato nosso e-mail dissottidevsoft@gmail.com

# Sobre #
A seguir ser apresentado informações relevantes sobre o Dissotti Software Livre:

### Licença GPL v3 ###

Dissotti é um software livre; você pode redistribuí-lo e/ou modificá-lo dentro dos termos da Licença Pública Geral GNU como publicada pela Fundação do Software Livre (FSF); na versão 3 da Licença, ou (na sua opinião) qualquer versão.

Este programa é distribuído na esperança de que possa ser  útil, mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
   
Licença Pública Geral GNU para maiores detalhes.
Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa, Se não, veja <http://www.gnu.org/licenses/>.

### Características ###
Atualmente, os seguintes recursos são implementados:

# Usando a Plataforma #

- **Camada de Apresentação**

  - Folhas de Estilo CSS
  - Componentes JavaScript
  - Mídia

- **Camada de Controle**

  - Exemplo Arquivos PHP

- **Camada de Dados**

  - Exemplo Entidades
  - Exemplo Relacionamentos

- **Ferramentas Utilizadas**

  - Microsoft Visual Studio Code - Insiders: https://code.visualstudio.com/license    
  - Materialize CSS: http://materializecss.com/getting-started.html
  

# Agradecimentos #

Agradecemos especialmente a

  - Marcos Dissotti do Nascimento Rodrigues - marcosdissotti@gmail.com

  - Gabriel Dissotti do Nascimento Rodrigues - gabrieldnrodrigues@gmail.com