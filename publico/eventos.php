<!DOCTYPE html>
<!--********************************************************************************************************
    Copyright 2016 Dissotti 
    
    Colaboradores:
    				Marcos Dissotti do Nascimento Rodrigues - marcosdissotti@gmail.com
    				Gabriel Dissotti do Nascimento Rodrigues - gabrieldnrodrigues@gmail.com

    Este arquivo é parte do programa Dissotti

    Dissotti é um software livre; você pode redistribuí-lo e/ou 
    modificá-lo dentro dos termos da Licença Pública Geral GNU como 
    publicada pela Fundação do Software Livre (FSF); na versão 3 da 
    Licença, ou (na sua opinião) qualquer versão.

    Este programa é distribuído na esperança de que possa ser  útil, 
    mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
    a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
    Licença Pública Geral GNU para maiores detalhes.

    Você deve ter recebido uma cópia da Licença Pública Geral GNU junto
    com este programa, Se não, veja <http://www.gnu.org/licenses/>.
    
************************************************************************************************************-->
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
		<meta name="description" content="Somos a solução que cabe no bolso do empreendedor, assim crescemos juntos com o seu negócio!">
		<!-- Sempre força o mais recente mecanismo de renderização do IE (mesmo na intranet) e Chrome Frame Remova isso se você usar o .htaccess-->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>Dissotti</title>
		<meta name="keywords" content="informática, informatica, websites, freelencer, redes, computadores, sistemas, automação, comercial, midia, educação, inovação, ciência">
		<meta name="author" content="Dissotti Desenvolvimento de Software">
		<meta name="google-site-verification" content="adicionar código de autenticação do Google" />
		<meta name="rebots" content="Serviços, Artigos, Sobre">
		<!-- define que a escala 100% é a largura da janela de exibição -->
		<meta name="viewport" content="initial-scale=1.0">

        <!-- Estilo Padrão -->
        <link href='css/estilo.css' rel='stylesheet' type='text/css'/>
        <!-- Materialize -->
        <link href='css/materialize.min.css' rel='stylesheet'/>
        <!-- Material Icons -->
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
    </head>
    <body class=' grey lighten-2'>
        <div class='conteudo'> 
            <!-- estrutura do site -->
            <?php
                include_once("header.php");
            ?>
            <div class='corpo' id='corpo'>
                <div class='corpo container'>
                <h1 class='center-align grey-text text-darken-1
'>Página em desenvolvimento</h1>
                    <div class="row responsive-text">
                        <div class=" col s12 m6 l4">
                            <div class='card small'>
                            <div class="card-image waves-effect waves-block waves-light">
                            <img class="activator" src="midia/office-605503_640.jpg">
                            </div>
                            <div class="card-content">
                            <span class="card-title activator blue-grey-text text-darken-2">Websites<i class="material-icons right">more_vert</i></span>
                            <p><a href="#">Solicitar Serviço</a></p>
                            </div>
                            <div class="card-reveal">
                            <span class="card-title purple-text text-lighten-1 ">Desenvolvimento de Websites<i class="material-icons right">close</i></span>
                            <p>Deseja investir no marketing on-line de sua empresa? Obtenha um website Oficial para o seu negócio</p>
                            </div>
                            </div>
                        </div>
                        <div class=" col s12 m6 l4">
                            <div class='card small'>
                            <div class="card-image waves-effect waves-block waves-light">
                            <img class="activator" src="midia/office-1209640_640.jpg">
                            </div>
                            <div class="card-content">
                            <span class="card-title activator blue-grey-text text-darken-2">Sistemas<i class="material-icons right">more_vert</i></span>
                            <p><a href="#">Solicitar Serviço</a></p>
                            </div>
                            <div class="card-reveal">
                            <span class="card-title blue-grey-text text-darken-2">Invista em um Sistema<i class="material-icons right">close</i></span>
                            <p>Agora você pode ter um sistema adaptado ao seu negócio e do seu gosto.</p>
                            </div>
                            </div>
                        </div>
                        <div class="col s12 m6 l4">
                            <div class='card small'>
                            <div class="card-image waves-effect waves-block waves-light">
                            <img class="activator" src="midia/service-428540_640.jpg">
                            </div>
                            <div class="card-content">
                            <span class="card-title activator blue-grey-text text-darken-2">Manutenção<i class="material-icons right">more_vert</i></span>
                            <p><a href="#">Solicitar Serviço</a></p>
                            </div>
                            <div class="card-reveal">
                            <span class="card-title blue-grey-text text-darken-2">Manutenção<i class="material-icons right">close</i></span>
                            <p>Tire aquele computador empoeirado do sótão, elimine a travação e o barulho do seu micro.</p>
                            </div>
                            </div>
                        </div>
                        <div class="col s12 m6 l4">
                            <div class='card small'>
                            <div class="card-image waves-effect waves-block waves-light">
                            <img class="activator" src="midia/office-1516329_640.jpg">
                            </div>
                            <div class="card-content">
                            <span class="card-title activator blue-grey-text text-darken-2">Consultoria<i class="material-icons right">more_vert</i></span>
                            <p><a href="#">Solicitar Serviço</a></p>
                            </div>
                            <div class="card-reveal">
                            <span class="card-title yellow-text text-darken-4">Consultoria<i class="material-icons right">close</i></span>
                            <p>Em dúvida sobre a segurança de um sistema ou a montagem de um computador? Tire suas dúvidas.</p>
                            </div>
                            </div>
                        </div>
                        <div class="col s12 m6 l4">
                            <div class='card small'>
                            <div class="card-image waves-effect waves-block waves-light">
                            <img class="activator" src="midia/graphic-1714230_640.png">
                            </div>
                            <div class="card-content">
                            <span class="card-title activator  blue-grey-text text-darken-2">Suporte<i class="material-icons right">more_vert</i></span>
                            <p><a href="#">Solicitar Serviço</a></p>
                            </div>
                            <div class="card-reveal">
                            <span class="card-title amber-text text-accent-3">Suporte<i class="material-icons right">close</i></span>
                            <p>Mantemos a sua aplicação disponível por você</p>
                            </div>
                            </div>
                        </div>
                        <div class="col s12 m6 l4">
                            <div class='card small'>
                            <div class="card-image waves-effect waves-block waves-light">
                            <img class="activator" src="midia/network-782707_640.png">
                            </div>
                            <div class="card-content">
                            <span class="card-title activator  blue-grey-text text-darken-2">Informatização<i class="material-icons right">more_vert</i></span>
                            <p><a href="#">Solicitar Serviço</a></p>
                            </div>
                            <div class="card-reveal">
                            <span class="card-title  card-title activator  teal-text text-accent-4">Informatização<i class="material-icons right">close</i></span>
                            <p>Está na hora de ter uma visão mais apurada de sua empresa através de um sistema, princípalmente para a tomada de decisões.</p>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
                include_once("footer.php");
            ?>
            <script type="text/javascript" src="https://platform.linkedin.com/badges/js/profile.js" async defer></script>
    </body>
</html>