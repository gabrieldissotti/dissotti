<!--********************************************************************************************************
    Copyright 2016 Dissotti 
    
    Colaboradores:
    				Marcos Dissotti do Nascimento Rodrigues - marcosdissotti@gmail.com
    				Gabriel Dissotti do Nascimento Rodrigues - gabrieldnrodrigues@gmail.com

    Este arquivo é parte do programa Dissotti

    Dissotti é um software livre; você pode redistribuí-lo e/ou 
    modificá-lo dentro dos termos da Licença Pública Geral GNU como 
    publicada pela Fundação do Software Livre (FSF); na versão 3 da 
    Licença, ou (na sua opinião) qualquer versão.

    Este programa é distribuído na esperança de que possa ser  útil, 
    mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
    a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
    Licença Pública Geral GNU para maiores detalhes.

    Você deve ter recebido uma cópia da Licença Pública Geral GNU junto
    com este programa, Se não, veja <http://www.gnu.org/licenses/>.
    
************************************************************************************************************-->
<footer class='rodape row blue-grey darken-4 valign-wrapper'>
        <!-- rodapé -->
        <div class='col s4 m6 l6'>
            <nav class='custom valign'>
                <ul class='vSeparado'>
                    <a href='#'><li><span class="sublinhado">Ajuda</span></li>    </a>
                    <a href='#'><li><span class="sublinhado">Contato</span></li>    </a>
                    <a href='licenca.php'><li><span class="sublinhado" >Licença</span></li>   </a>
                </ul>
            </nav>
        </div>
        <div class="col s12 m6 l4">
                <img src="midia/vlogo.png" class='responsive-img' alt='Logo Dissotti Software Livre'>
        </div>
        <div class='col s4 m6 l4valign'>
            <p class='grey-text text-lighten-3 right valign'><span class="negrito">&copy; 2016 Dissotti Software Development</span></p>
        </div>     
    </footer>
</div>     
<!-- Jquery -->
<script src='js/jquery-3.1.1.min.js'></script>  
<!-- Materialize -->
<script src='js/materialize.min.js'></script> 
<!-- outros scripts -->  
<script src='js/functions.js'></script>

<script>
//iniciar um sideNav com os parâmentros informados
    new_sideNav(300, 'left',false,true,true, 500);
</script>