<!--********************************************************************************************************
    Copyright 2016 Dissotti 
    
    Colaboradores:
    				Marcos Dissotti do Nascimento Rodrigues - marcosdissotti@gmail.com
    				Gabriel Dissotti do Nascimento Rodrigues - gabrieldnrodrigues@gmail.com

    Este arquivo é parte do programa Dissotti

    Dissotti é um software livre; você pode redistribuí-lo e/ou 
    modificá-lo dentro dos termos da Licença Pública Geral GNU como 
    publicada pela Fundação do Software Livre (FSF); na versão 3 da 
    Licença, ou (na sua opinião) qualquer versão.

    Este programa é distribuído na esperança de que possa ser  útil, 
    mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
    a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
    Licença Pública Geral GNU para maiores detalhes.

    Você deve ter recebido uma cópia da Licença Pública Geral GNU junto
    com este programa, Se não, veja <http://www.gnu.org/licenses/>.
    
************************************************************************************************************-->
<header class='cabecalho row blue-grey darken-4'>
    <!-- cabeçalho -->
    <div class='valign-wrapper'>
        <a href="#" data-activates="slide-out" class="button-collapse valign col s1"><i class="material-icons white-text small left">menu</i></a>
        <nav class='custom valign col s10'>
            <ul class='headervSeparado row' id='ulcabecalho'>                            
                <a href='index.php'><li class='valign-wrapper hide ' id='inicio'><i class="material-icons medium left rightMargin">home</i>Início</li></a>
                <a href='projetossociais.php'><li class='valign-wrapper hide '  id='projetos'><i class="material-icons medium left rightMargin">people</i>Projetos Sociais</li></a>
                <a href='#' ><li class='dropdown-button valign-wrapper hide ' data-beloworigin="true"  data-activates='dropdown_servicos'id='servicos'style='width:190px;'><i class="material-icons medium left rightMargin">work</i>Serviços</li>   </a>
                <!-- Dropdown Structure -->
                <ul id='dropdown_servicos' class='dropdown-content green lighten-5' >
                    <li style='margin-left:0px;'><a href="servicos.php"><i class="material-icons left">view_module</i>Servicos</a></li>
                    <li  style='margin-left:0px;' class="divider"></li>
                    <li  style='margin-left:0px;'><a href="assistentemanutencao.php"><i class="material-icons left" >laptop_windows</i>Manutenção</a></li>
                    <li style='margin-left:0px;'><a href="#!"><i class="material-icons left">web</i>Websites</a></li>
                    <li style='margin-left:0px;'><a href="#!"><i class="material-icons left">web_asset</i>Sistemas</a></li>
                    <li  style='margin-left:0px;'><a href="#!"><i class="material-icons left">settings_system_daydream</i>Informatização</a></li>
                    <li style='margin-left:0px;'><a href="#!"><i class="material-icons left">thumb_up</i>Consultoria</a></li>
                    <li  style='margin-left:0px;'><a href="#!"><i class="material-icons left">supervisor_account</i>Suporte</a></li>
                </ul>
                <a href='loja.php'><li class='valign-wrapper right hide' id='loja'><i class="material-icons medium left rightMargin">store</i>Loja</li>   </a>
            </ul>
            
        </nav>
    </div>     
</header>

<!-- Side Nav -->    
<ul id="slide-out" class="side-nav blue-grey darken-4">
    <li><div class="userView">
    <div class="background backSidNav">
        <img src="midia/vlogo.png" class='responsive-img' alt='Logo Dissotti Software Development'>
    </div>
    <br><br>
    </div></li>
    <nav class='custom blue-grey darken-3'>
        <div class="nav-wrapper">
        <form>
            <div class="input-field">
                <input id="search" type="search" required>
                <label for="search"><i class="material-icons">search</i></label>
                <i class="material-icons">close</i>
            </div>
        </form>
        </div>
    </nav>
    <li><a href="index.php"  class='modifica blue-grey-text text-lighten-4 waves-effect customSidNav'><i class="material-icons blue-grey-text text-lighten-4 small">home</i>Início</a></li>
    <li><a href="projetossociais.php"  class='modifica blue-grey-text text-lighten-4 waves-effect customSidNav'><i class="material-icons blue-grey-text text-lighten-4 small">people</i>Projetos Sociais</a></li>
    <li><a href="#!"  class='modifica blue-grey-text text-lighten-4 waves-effect customSidNav'><i class="material-icons blue-grey-text text-lighten-4 small">library_books</i>Artigos</a></li>
    <li><a href="loja.php"  class='modifica blue-grey-text text-lighten-4 waves-effect customSidNav'><i class="material-icons blue-grey-text text-lighten-4 small">store</i>Loja</a></li>
    <li><a href="eventos.php"  class='modifica blue-grey-text text-lighten-4 waves-effect customSidNav'><i class="material-icons blue-grey-text text-lighten-4 small">events</i>Eventos</a></li>
    <li><a href="servicos.php"  class='modifica blue-grey-text text-lighten-4 waves-effect customSidNav'><i class="material-icons blue-grey-text text-lighten-4 small">work</i>Serviços</a></li>
    <li><a href="sobre.php"  class='modifica blue-grey-text text-lighten-4 waves-effect customSidNav'><i class="material-icons blue-grey-text text-lighten-4 small">info_outline</i>Sobre</a></li>    
    <li><div class="divider blue-grey darken-2"></div></li>
    <li><a class="subheader blue-grey-text text-lighten-4 waves-effect">Visite-nos também no:</a></li>
    <li ><a href="https://www.facebook.com/dissottidev" target='_blank' class='blue-grey-text text-lighten-4 waves-effect'><img src='midia/facebook.png' class='icone'>Facebook</a></li>
    <li ><a href="https://youtu.be/TfpY_v6ooXI" target='_blank' class='blue-grey-text text-lighten-4 waves-effect'><img src='midia/youtube.png' class='icone'>Youtube</a></li>
    <li ><a href="repositorios.php"  class='blue-grey-text text-lighten-4 waves-effect'><img src='midia/github.png' class='icone'>GitHub</a></li>
    <br><br>
</ul>