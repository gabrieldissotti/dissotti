<!DOCTYPE html>
<!--********************************************************************************************************
    Copyright 2016 Dissotti 
    
    Colaboradores:
    				Marcos Dissotti do Nascimento Rodrigues - marcosdissotti@gmail.com
    				Gabriel Dissotti do Nascimento Rodrigues - gabrieldnrodrigues@gmail.com

    Este arquivo é parte do programa Dissotti

    Dissotti é um software livre; você pode redistribuí-lo e/ou 
    modificá-lo dentro dos termos da Licença Pública Geral GNU como 
    publicada pela Fundação do Software Livre (FSF); na versão 3 da 
    Licença, ou (na sua opinião) qualquer versão.

    Este programa é distribuído na esperança de que possa ser  útil, 
    mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
    a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
    Licença Pública Geral GNU para maiores detalhes.

    Você deve ter recebido uma cópia da Licença Pública Geral GNU junto
    com este programa, Se não, veja <http://www.gnu.org/licenses/>.
    
************************************************************************************************************-->
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
		<meta name="description" content="Somos a solução que cabe no bolso do empreendedor, assim crescemos juntos com o seu negócio!">
		<!-- Sempre força o mais recente mecanismo de renderização do IE (mesmo na intranet) e Chrome Frame Remova isso se você usar o .htaccess-->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>Dissotti</title>
		<meta name="keywords" content="informática, informatica, websites, freelencer, redes, computadores, sistemas, automação, comercial, midia, educação, inovação, ciência">
		<meta name="author" content="Dissotti Desenvolvimento de Software">
		<meta name="google-site-verification" content="adicionar código de autenticação do Google" />
		<meta name="rebots" content="Serviços, Artigos, Sobre">
		<!-- define que a escala 100% é a largura da janela de exibição -->
		<meta name="viewport" content="initial-scale=1.0">

        <!-- Estilo Padrão -->
        <link href='css/estilo.css' rel='stylesheet' type='text/css'/>
        <!-- Materialize -->
        <link href='css/materialize.min.css' rel='stylesheet'/>
        <!-- Material Icons -->
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
    </head>
    <body class=' indigo blue lighten-5'>
        <div class='conteudo'> 
            <!-- estrutura do site -->
            <?php
                include_once("header.php");
            ?>
            <div class='corpo' id='corpo'>
            <!-- Corpo -->
                <div>
                    <div class="row responsive-text">
                        <div class="slider">
                            <ul class="slides">
                            <li>
                                <img src="midia/banners/itape.min.jpg"> <!-- random image -->
                                <div class="caption rigth-align">
                                <h2>Organização</h3>
                                <h5 class="light grey-text text-lighten-3">Seu negócio nunca mais será o mesmo.</h5>
                                <a href='assistentemanutencao.php' class="waves-effect waves-light deep-purple darken-4 btn"><i class="material-icons left">check</i>Solicitar Serviço</a>
                                </div>
                            </li>
                            <li>
                                <img src="midia/banners/peixoto.min.jpg"> <!-- random image -->
                                <div class="caption left-align">
                                <h2>Comunidade</h2>
                                <h5 class="light grey-text text-lighten-3">Sinônimo de progresso é a Interação.</h5>
                                <a class="waves-effect waves-light yellow darken-4 btn"><i class="material-icons left">language</i>Contribuir</a>
                                </div>
                            </li>
                             <li>
                                <img src="midia/banners/prefeitura.min.jpg"> <!-- random image -->
                                <div class="caption right-align">
                                <h2>Inovação</h2>
                                <h5 class="light grey-text text-lighten-3">Problemas são oportunidades.</h5>
                                <a class="waves-effect waves-light light-blue darken-3 btn"><i class="material-icons left">cloud</i>Consultar</a>
                                </div>
                            </li>
                            <li>
                                <img src="midia/banners/for.min.jpg"> <!-- random image -->
                                <div class="caption right-align">
                                <h2>Análise</h2>
                                <h5 class="light grey-text text-lighten-3">Uma perspectiva metodológica.</h5>
                                <a class="waves-effect waves-light light-green darken-3 btn"><i class="material-icons left">thumb_up</i>Solicitar Análise</a>
                                </div>
                            </li>
                            <li>
                                <img src="midia/banners/jap.min.jpg"> <!-- random image -->
                                <div class="caption center-align">
                                <h3>Artigos</h3>
                                <h5 class="light grey-text text-lighten-3">A visão holística, implica a curiosidade.</h5>
                                <a class="waves-effect waves-light red darken-3 btn"><i class="material-icons left">library_books</i>Ler artigos</a>
                                </div>
                            </li>
                            </ul>
                        </div>
                        <div class='imagem_fade' style='opacity:0;'>
                            <section class='col s12 m6 l8 '> 
                                                                                    
                                <h2 class='left-align grey-text text-darken-1 fonte_banner titulo_coluna_noticia'>Destaques</h2>
                                <!-- ARTIGO -->                       
                                <article class=" col s12 m12 l12">
                                    <div class='card large'>
                                    <div class="card-image waves-effect waves-block waves-light">
                                    <img class="activator" src="midia/office-1209640_640.jpg">
                                    </div>
                                    <div class="card-content">
                                    <span class="card-title activator blue-grey-text text-darken-2">A empresa Su Doces e Bolos de Sorocaba está negociando sua nova fonte de sucesso e reconhecimento, seu primeiro Website, faça o mesmo!
                                    <p><a href="planejadosdissotti.com.br">Saiba mais...</a></p>
                                    </div>                                
                                    </div>
                                </article>
                                <!-- ARTIGO -->
                                <article class=" col s12 m6 l6">
                                    <div class='card small'>
                                    <div class="card-image waves-effect waves-block waves-light">
                                    <img class="activator" src="midia/faceDissottiDevSoft.png">
                                    </div>
                                    <div class="card-content">
                                    <span class="card-title activator blue-grey-text text-darken-2">Nossa Página do Facebook<i class="material-icons right">more_vert</i></span>
                                    <p><a href="#">Ver agora...</a></p>
                                    </div>
                                    <div class="card-reveal">
                                    <span class="card-title purple-text text-lighten-1 ">Desenvolvimento de Websites<i class="material-icons right">close</i></span>
                                    <p>Deseja investir no marketing on-line de sua empresa? Obtenha um website Oficial para o seu negócio</p>
                                    </div>
                                    </div>
                                </article>
                                <!-- ARTIGO -->
                                <article class=" col s12 m6 l6">
                                    <div class='card small'>
                                    <div class="card-image waves-effect waves-block waves-light">
                                    <img class="activator" src="midia/planejadosdissotti.png">
                                    </div>
                                    <div class="card-content">
                                    <span class="card-title activator blue-grey-text text-darken-2">A empresa Planejados Dissotti.<i class="material-icons right">more_vert</i></span>
                                    <p><a href="#">Ler mais...</a></p>
                                    </div>
                                    <div class="card-reveal">
                                    <span class="card-title purple-text text-lighten-1 ">sas <i class="material-icons right">close</i></span>
                                    <p>A empresa Planejados Dissotti adquiriu recentemente um Website Oficial para o seu negócio. Deseja investir no marketing on-line de sua empresa? Obtenha um website Oficial para o seu negócio</p>
                                    </div>
                                    </div>
                                </article>
                            </section>
                            <asid class='col s12 m6 l4 '>
                                
                                <h2 class='left-align grey-text fonte_banner text-darken-1 titulo_coluna_noticia'>Promoções</h2>
                                <!-- ARTIGO -->
                                <article class=" col s12 m12 l12">
                                    <div class='card'>
                                    <div class="card-image waves-effect waves-block waves-light">
                                    <div class="slider">
                                <ul class="slides">

                                <li>
                                    <img src="midia/mouse4.jpg"> <!-- random image -->
                                    <div class="caption left-align">
                                    <h3 class="light  teal-text text-lighten-1">Wi Fi</h3>
                                    <h5 class="light teal-text text-lighten-2">Sem Fio</h5>
                                    </div>
                                </li>
                                <li>
                                    <img src="midia/mouse.jpg"> <!-- random image -->
                                    <div class="caption center-align">
                                    <h3 class="light red-text  text-accent-1">Até 15m²</h3>
                                    <h5 class="light red-text text-lighten-1">Longa Distância</h5>
                                    </div>
                                </li>
                                <li>
                                    <img src="midia/mouse2.jpg"> <!-- random image -->
                                    <div class="caption right-align">
                                    <h3>3200DPI</h3>
                                    <h5 class="light red-text  text-accent-1">Alta Qualidade</h5>
                                    </div>
                                </li>
                                <li>
                                    <img src="midia/mouse3.jpg"> <!-- random image -->
                                    <div class="caption center-align">
                                    <h5 class="light red-text text-accent-1">De: <span class='tracado'>R$75,00</span></h5>
                                    <h3 class="light green-text text-lighten-1">Por R$30,00</h3>
                                    </div>
                                </li>
                                </ul>
                            </div>
                                    </div>
                                    <div class="card-content">
                                    <span class="card-title activator blue-grey-text text-darken-2">Mouse Sem Fio</span>
                                    <p><a href="#">COMPRAR</a></p>
                                    </div>
                                    </div>
                                </article>
                            </asid>
                        </div>
                    </div>

                </div>   
            </div>
            <?php
                include_once("footer.php");
            ?>
            <script>
            
            //iniciar um slider com os parâmentros informados
            new_slider('.cabecalho',0,false);



            var options = [
                {selector: '.conteudo', offset: 0, callback: function(el) {
                Materialize.toast("Olá! Tudo bem? Já nos conhece?", 3000 );
                Materialize.toast("<a href='sobre.php' class='blue-text text-lighten-4'> Clique para saber mais sobre a Dissotti</a>", 4000 ); 
                } },
                {selector: '.imagem_fade', offset: 400, callback: function(el) {
                Materialize.fadeInImage($(el));
                } }
                ];
            Materialize.scrollFire(options);
            </script>
    </body>
</html>