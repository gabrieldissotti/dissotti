<!DOCTYPE html>
<!--********************************************************************************************************
    Copyright 2016 Dissotti 
    
    Colaboradores:
    				Marcos Dissotti do Nascimento Rodrigues - marcosdissotti@gmail.com
    				Gabriel Dissotti do Nascimento Rodrigues - gabrieldnrodrigues@gmail.com

    Este arquivo é parte do programa Dissotti

    Dissotti é um software livre; você pode redistribuí-lo e/ou 
    modificá-lo dentro dos termos da Licença Pública Geral GNU como 
    publicada pela Fundação do Software Livre (FSF); na versão 3 da 
    Licença, ou (na sua opinião) qualquer versão.

    Este programa é distribuído na esperança de que possa ser  útil, 
    mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
    a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
    Licença Pública Geral GNU para maiores detalhes.

    Você deve ter recebido uma cópia da Licença Pública Geral GNU junto
    com este programa, Se não, veja <http://www.gnu.org/licenses/>.
    
************************************************************************************************************-->
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
		<meta name="description" content="Somos a solução que cabe no bolso do empreendedor, assim crescemos juntos com o seu negócio!">
		<!-- Sempre força o mais recente mecanismo de renderização do IE (mesmo na intranet) e Chrome Frame Remova isso se você usar o .htaccess-->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>Dissotti</title>
		<meta name="keywords" content="informática, informatica, websites, freelencer, redes, computadores, sistemas, automação, comercial, midia, educação, inovação, ciência">
		<meta name="author" content="Dissotti Desenvolvimento de Software">
		<meta name="google-site-verification" content="adicionar código de autenticação do Google" />
		<meta name="rebots" content="Serviços, Artigos, Sobre">
		<!-- define que a escala 100% é a largura da janela de exibição -->
		<meta name="viewport" content="initial-scale=1.0">

        <!-- Estilo Padrão -->
        <link href='css/estilo.css' rel='stylesheet' type='text/css'/>
        <!-- Materialize -->
        <link href='css/materialize.min.css' rel='stylesheet'/>
        <!-- Material Icons -->
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
    </head>
    <body class=' grey lighten-2'>
        <div class='conteudo'> 
            <!-- estrutura do site -->
            <?php
                include_once("header.php");
            ?>
            <div class='corpo' id='corpo'>
                <h1 class='center-align blue-text text-lighten-2 '>Dissotti Software Development</h1>
                <div class='corpo container'>
                <!-- Corpo -->
                    <section>
                        <div class="row">
                            <div class="">
                                <div class="card-content responsive-text center-align">
                                    <p class=''>Nossa equipe trás soluções ainda mais sofisticadas para pequenos negócios, oferecendo padrões de produtos livres de custos e serviços com um custo-benefício surpreendente.</p>
                                    <br>
                                    <p>A Dissotti Software Development surgiu em 2016, na cidade de Itapetininga e foi cofundada por Marcos Dissotti e Gabriel Dissotti. Sua principal missão é tornar a tecnologia acessível a todos, incluindo você e o seu negócio.</p>
                                    <div class='row center'>
                                        <h2 class='blue-text text-lighten-2 '>Conheça a Equipe</h2>
                                        <figure>
                                            <img src=''>
                                            <ficaption>[Foto da Equipe]</ficaption>
                                        </figure>
                                        <p>Saiba mais sobre nossos profissionais</p>
                                        <div class=" margem-topo LI-profile-badge col s12 m12 l6"  data-version="v1" data-size="large" data-locale="pt_BR" data-type="vertical" data-theme="dark" data-vanity="gabriel-dissotti-do-nascimento-rodrigues-b90514124"><a class="LI-simple-link" href='https://br.linkedin.com/in/gabriel-dissotti-do-nascimento-rodrigues-b90514124?trk=profile-badge' target='_blank'>Gabriel Dissotti</a></div>
                                        
                                        <div class="margem-topo LI-profile-badge col s12 m12 l6"  data-version="v1" data-size="large" data-locale="pt_BR" data-type="vertical" data-theme="dark" data-vanity="marcos-dissotti-706810117"><a class="LI-simple-link" href='https://www.linkedin.com/in/marcos-dissotti-706810117' target='_blank'>Marcos Dissotti</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <?php
                include_once("footer.php");
            ?>
            <script type="text/javascript" src="https://platform.linkedin.com/badges/js/profile.js" async defer></script>
            <script>
                jQuery(function($) {
                    //forçar o rodapé a ser relativo por causa dos crachas...
                    $('.rodape').css("position", 'relative');
                });
            </script>
    </body>
</html>