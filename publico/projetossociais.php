<!DOCTYPE html>
<!--********************************************************************************************************
    Copyright 2016 Dissotti 
    
    Colaboradores:
    				Marcos Dissotti do Nascimento Rodrigues - marcosdissotti@gmail.com
    				Gabriel Dissotti do Nascimento Rodrigues - gabrieldnrodrigues@gmail.com

    Este arquivo é parte do programa Dissotti

    Dissotti é um software livre; você pode redistribuí-lo e/ou 
    modificá-lo dentro dos termos da Licença Pública Geral GNU como 
    publicada pela Fundação do Software Livre (FSF); na versão 3 da 
    Licença, ou (na sua opinião) qualquer versão.

    Este programa é distribuído na esperança de que possa ser  útil, 
    mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
    a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
    Licença Pública Geral GNU para maiores detalhes.

    Você deve ter recebido uma cópia da Licença Pública Geral GNU junto
    com este programa, Se não, veja <http://www.gnu.org/licenses/>.
    
************************************************************************************************************-->
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
		<meta name="description" content="Somos a solução que cabe no bolso do empreendedor, assim crescemos juntos com o seu negócio!">
		<!-- Sempre força o mais recente mecanismo de renderização do IE (mesmo na intranet) e Chrome Frame Remova isso se você usar o .htaccess-->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>Dissotti</title>
		<meta name="keywords" content="informática, informatica, websites, freelencer, redes, computadores, sistemas, automação, comercial, midia, educação, inovação, ciência">
		<meta name="author" content="Dissotti Desenvolvimento de Software">
		<meta name="google-site-verification" content="adicionar código de autenticação do Google" />
		<meta name="rebots" content="Serviços, Artigos, Sobre">
		<!-- define que a escala 100% é a largura da janela de exibição -->
		<meta name="viewport" content="initial-scale=1.0">

        <!-- Estilo Padrão -->
        <link href='css/estilo.css' rel='stylesheet' type='text/css'/>
        <!-- Materialize -->
        <link href='css/materialize.min.css' rel='stylesheet'/>
        <!-- Material Icons -->
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
    </head>
    <body class=' grey lighten-2'>
        <div class='conteudo'> 
            <!-- estrutura do site -->
            <?php
                include_once("header.php");
            ?>
            <div class='corpo' id='corpo'>
            <!-- Corpo -->
                <div>
                    <!-- SESSÃO de SLOGAN da Página Inícial -->
                    <section>
                        <div class="slogan card-painel grey lighten-4">
                            <p class="responsive-text center">
                                <span class="negrito">Dissotti Software Livre</span>
                                é uma inciativa dos técnicos <span class="negrito">Marcos</span> e <span class="negrito">Gabriel</span> que 
                                permite a comunidade apresentar propostas de software para melhorar a qualidade de vida 
                                das pessoas com a tecnologia, colabore você também <a href="https://github.com/MarcosDissotti/dissotti"><span class="sublinhado negrito">desenvolvendo<span></a> a aplicação ou 
                                <a href="#"><span class="sublinhado negrito"> envie </span></a> seu projeto!
                            </p>
                        </div>
                    </section>
                    <div class="container">
                        <!-- SESSÃO do conteúdo TEXTUAL-->
                        <section>
                            <div class="row">
                                <div class="card">
                                    <div class="card-content responsive-text">
                                        <article>
                                            <h1 class="header">Projetos Sociais</h1> 
                                            <hr>
                                            <br>
                                            <p class='identado responsive-text justificado'>
                                                O Projeto <span class="negrito ">Dissotti Software Livre</span>  tem por objetivo recolher e implementar projetos livres de custos em virtude da melhor qualidade de vida da sociedade. Para <span class="negrito">submeter um projeto</span>, selecione uma das opções de semelhança ao seu perfil: <span class="negrito grey-text text-darken-3" style="font-size: 20pt">Você é um/a?</span>
                                            </p>
                                            <br>
                                            <div class="row">
                                                <a href="#" class="grey-text text-darken-3">
                                                    <div class="col s12 m6 l4"> <!-- Linha com 3 colunas de Cartões -->
                                                        <div class="card deep-orange lighten-3 hoverable">
                                                            <div class="card-image center">
                                                                <i class="material-icons responsive-text" style="font-size: 100pt">account_balance</i>
                                                            </div>
                                                            <div class="card-content center">
                                                                <span class="card-title black-text responsive-text sublinhado">Servidor</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                                <a href="#" class="grey-text text-darken-3">
                                                    <div class="col s12 m6 l4"> <!-- Linha com 3 colunas de Cartões -->
                                                        <div class="card light-blue lighten-3 hoverable">
                                                            <div class="card-image center">
                                                                <i class="material-icons responsive-text" style="font-size: 100pt">school</i>
                                                            </div>
                                                            <div class="card-content center">
                                                                <span class="card-title black-text responsive-text sublinhado">Estudante</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                                <a href="#" class="grey-text text-darken-3">
                                                    <div class="col s12 m6 l4"> <!-- Linha com 3 colunas de Cartões -->
                                                        <div class="card green lighten-3 hoverable">
                                                            <div class="card-image center">
                                                                <i class="material-icons responsive-text" style="font-size: 100pt">monetization_on</i>
                                                            </div>
                                                            <div class="card-content center">
                                                                <span class="card-title black-text responsive-text sublinhado">Empreendedor/a</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                                <a href="#" class="grey-text text-darken-3">
                                                    <div class="col s12 m12 l12"> <!-- Linha com 3 colunas de Cartões -->
                                                        <div class="card yellow lighten-3 hoverable">
                                                            <div class="card-image center">
                                                                <i class="material-icons responsive-text sublinhado" style="font-size: 100pt">code</i>
                                                            </div>
                                                            <div class="card-content center">
                                                                <span class="card-title black-text responsive-text sublinhado">Desenvolvedor/a</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>                                                
                                                <div class="col s12 m12 l12">
                                                <div class="card">
                                                    <h2 class="header responsive-text">Estranho?</h2>
                                                    <div class="card-image center">
                                                        <i class="material-icons responsive-text" style="font-size: 150pt;">lightbulb_outline</i>
                                                    <div class="card-content">
                                                        <p class=" identado responsive-text justificado">
                                                            <span class="negrito">Isso mesmo</span>, você estudante que deseja promover alguma solução para seu campus/escola, talvez empreendedor que procura por um sistema para gerenciar vendas, ou paciente que enfrenta uma enorme fila para ser atendido em um posto de saúde, ou mesmo um profissional liberal como um médico em seu consultório ou um advogado em seu escritório, agora podem sugerir projetos isentos de custos ou utilizar de nossas
                                                            <a href='#' class='sublinhado'> aplicações já publicadas</a>.
                                                        </p>
                                                    </div>
                                                        <div class="card-action">
                                                            <a href="#"><span class="negrito sublinhado"> Submeta o seu projeto...</span></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </article>
                                </div>
                            </div>
                        </section>
                        <!-- SESSÃO dos CARTÕES Visitas-->
                        <section>
                            <div class="row">
                                <div class="col s12 m6 l4"> <!-- Linha com 3 colunas de Cartões -->
                                    <div class="card">
                                        <div class="card-image">
                                            <img src="#">
                                            <span class="card-title">Título</span>
                                        </div>
                                        <div class="card-content">
                                                <p>
                                                    conteudo
                                                </p>
                                        </div>
                                        <div class="card-action">
                                            <a href="#">This is a link</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col s12 m6 l4">
                                    <div class="card">
                                        <div class="card-image">
                                            <img src="#">
                                            <span class="card-title">Título</span>
                                        </div>
                                        <div class="card-content">
                                                <p>
                                                    conteudo
                                                </p>
                                        </div>
                                        <div class="card-action">
                                            <a href="#">This is a link</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col s12 m6 l4">
                                    <div class="card">
                                        <div class="card-image">
                                            <img src="#">
                                            <span class="card-title">Título</span>
                                        </div>
                                        <div class="card-content">
                                                <p>
                                                    conteudo
                                                </p>
                                        </div>
                                        <div class="card-action">
                                            <a href="#">This is a link</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- SESSÃO de PAGINAÇÃO-->
                        <section>
                            <div class="row"><!-- Linha com Paginação-->
                                <div class="col s12 m12 l12 center">
                                    <ul class="pagination center">
                                        <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                                        <li class="active"><a href="#!">1</a></li>
                                        <li class="waves-effect"><a href="#!">2</a></li>
                                        <li class="waves-effect"><a href="#!">3</a></li>
                                        <li class="waves-effect"><a href="#!">4</a></li>
                                        <li class="waves-effect"><a href="#!">5</a></li>
                                        <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
                                    </ul>
                                </div>
                            </div>        
                        </section>
                    </div>
                </div>   
            </div>
            <?php
                include_once("footer.php");
            ?>
    </body>
</html>