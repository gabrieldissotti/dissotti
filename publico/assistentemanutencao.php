<!DOCTYPE html>
<!--********************************************************************************************************
    Copyright 2016 Dissotti 
    
    Colaboradores:
    				Marcos Dissotti do Nascimento Rodrigues - marcosdissotti@gmail.com
    				Gabriel Dissotti do Nascimento Rodrigues - gabrieldnrodrigues@gmail.com

    Este arquivo é parte do programa Dissotti

    Dissotti é um software livre; você pode redistribuí-lo e/ou 
    modificá-lo dentro dos termos da Licença Pública Geral GNU como 
    publicada pela Fundação do Software Livre (FSF); na versão 3 da 
    Licença, ou (na sua opinião) qualquer versão.

    Este programa é distribuído na esperança de que possa ser  útil, 
    mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO 
    a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
    Licença Pública Geral GNU para maiores detalhes.

    Você deve ter recebido uma cópia da Licença Pública Geral GNU junto
    com este programa, Se não, veja <http://www.gnu.org/licenses/>.
    
************************************************************************************************************-->
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
		<meta name="description" content="Somos a solução que cabe no bolso do empreendedor, assim crescemos juntos com o seu negócio!">
		<!-- Sempre força o mais recente mecanismo de renderização do IE (mesmo na intranet) e Chrome Frame Remova isso se você usar o .htaccess-->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>Dissotti</title>
		<meta name="keywords" content="informática, informatica, websites, freelencer, redes, computadores, sistemas, automação, comercial, midia, educação, inovação, ciência">
		<meta name="author" content="Dissotti Desenvolvimento de Software">
		<meta name="google-site-verification" content="adicionar código de autenticação do Google" />
		<meta name="rebots" content="Serviços, Artigos, Sobre">
		<!-- define que a escala 100% é a largura da janela de exibição -->
		<meta name="viewport" content="initial-scale=1.0">

        <!-- Estilo Padrão -->
        <link href='css/estilo.css' rel='stylesheet' type='text/css'/>
        <!-- Estilo do formulario de passo a passo -->
        <link href='css/esteps.css' rel='stylesheet' type='text/css'/>
        <!-- Materialize -->
        <link href='css/materialize.min.css' rel='stylesheet'/>
        <!-- Material Icons -->
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
    </head>
    <body class=' blue-grey darken-1'>
        <div class='conteudo'> 
            <!-- estrutura do site -->
            <?php
                include_once("header.php");
            ?>
            <style>.apagar{display:none;}</style>
            <div id='preloader' class="col s12">
                    <h1 class='white-text fonte_banner center-align'>A Realizar Orçamento Para Manutenção<br>
                        <i class="material-icons white-text" style="font-size: 300%;">computer</i>
                    </h1>
                    
            </div>
            <div class='corpo container' id='corpo'>
                
                <form style='display:none;' id="cliente_frm" class="frm-medio row" method="POST" enctype="multipart/form-data" action='../controle/manutencao.php' name='cliente_frm'>
                    <ul id="progress" class="" >
                        <li class="ativo">Localização</li>
                        <li>Serviço</li>
                        <li>Contato</li>
                    </ul>
                    <fieldset class="col s12 white">
                        <div class="resp center-align"></div>
                        <h2>Localização</h2>
                        <div class="row">
                            <div class="input-field col s12 m6 left-align">
                            <input id="cep" type="text" class="validate" name='cep'>
                            <label for="cep" >CEP<span class='red-text'> *</span></label>
                            </div>
                            <div class="input-field col s12 m6 left-align">
                            <input id="logradouro" type="text" class="validate" name='logradouro'>
                            <label for="logradouro">Número do logradouro<span class='red-text'> *</span></label>
                            </div>
                        </div>
                        <div class="input-field col s4 left-align">
                            <input id="complemento" type="text" class="validate" name='complemento'>
                            <label for="complemento">Complemento</label>
                        </div>  
                        <div class="input-field col s8 left-align">
                            <input id="bairro" type="text" class="validate" value=' ' name='bairro'>
                            <label for="bairro" class='active'>Bairro<span class='red-text'> *</span></label>
                        </div> 
                        <div class="input-field col s12 left-align">
                            <input id="rua" type="text" class="validate" value=' ' name='rua'>
                            <label for="rua" class='active'>Rua<span class='red-text'> *</span></label>
                        </div> 
                        <input type="button" name="next1" onclick="calcular();" class="next acao" value="Proxímo" />
				    </fieldset>
                        
                    <fieldset>
                        <div class="resp center-align" style='margin:-20px -20px 20px -20px;'></div>
                        <h2>Informações do Serviço</h2>
                        <div class="row responsive-text">
                            <p>
                                Selecione as opções que correspondem ao serviço que você deseja, caso não saíba, selecione a opção "Eu não sei..." e prossiga.
                            </p>
                            <p>
                                <input onclick="desativafiled('#des');ativafiled('#op1')" class="with-gap" name="group1" type="radio" value='1' id="test1"  value='op1' />
                                <label   for="test1">Eu não sei, desejo uma avaliação do disposítivo pela assistência.</label>
                            </p>
                            
                            <p>
                                <input class="with-gap" onclick="ativafiled('#des');desativafiled('#op1');" name="group1" type="radio" value='2' id="test2" value='op2' checked="checked"  />
                                <label for="test2">Desejo escolher.</label>
                            </p>
                        </div>
                        <!-- conteudo da primeira opção -->
                        <div class="row" id='op1'>
                            <p>Descreva em detalhes qual o problema com seu dispositivo.</p>
                            <div class="row">
                                <div class="input-field col s12">
                                    <textarea id="descricao" value='' name='descricao' class="materialize-textarea" length="500"></textarea>
                                    <label for="descricao">Descrição do problema<span class='red-text'> *</span></label>
                                </div>
                            </div>
                            <blockquote>Assinale todas as seguintes questões acerca do dispositivo. Respostas verídicas aceleram a resolução do problema.</blockquote>
                            <p class='grey-text text-darken-3'>1) Sofreu uma queda de mais de 15cm?<span class='red-text'> *</span></p>
                            <p>
                                <input class="" name="resp1" type="radio" id="resp1s" value='sim'  />
                                <label for="resp1s">Sim.</label>
                                <input class="" name="resp1" type="radio" id="resp1n" value='nao' />
                                <label for="resp1n">Não.</label>
                            </p>
                            <p class='grey-text text-darken-3'>2) Costuma pegar umidade ou caiu na água/molhou?<span class='red-text'> *</span></p>
                            <p>
                                <input class="" name="resp2" type="radio" id="resp2s" value='sim'  />
                                <label for="resp2s">Sim.</label>
                                <input class="" name="resp2" type="radio" id="resp2n"  value='nao'/>
                                <label for="resp2n">Não.</label>
                            </p>
                            <p class='grey-text text-darken-3'>3) Passou por limpeza preventiva dentro de 1 ano?<span class='red-text'> *</span></p>
                            <p>
                                <input class="" name="resp3" type="radio" id="resp3s"  value='sim' />
                                <label for="resp3s">Sim.</label>
                                <input class="" name="resp3" type="radio" id="resp3n" value='nao' />
                                <label for="resp3n">Não.</label>
                            </p>
                            <p class='grey-text text-darken-3'>4) O hardware foi aberto e manuseado por alguém?<span class='red-text'> *</span></p>
                            <p>
                                <input class="" name="resp4" type="radio" id="resp4s" value='sim'  />
                                <label for="resp4s">Sim.</label>
                                <input class="" name="resp4" type="radio" id="resp4n" value='nao'  />
                                <label for="resp4n">Não.</label>
                            </p>                            
                        </div>
                        <!-- conteudo da segunda opção -->
                        <div class='row' id='des'>
                            <table class='striped'>
                                <thead>
                                    <tr>
                                        <th data-field="id">Serviço</th>
                                        <th data-field="name">Preço</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr>
                                        <td>
                                            <input type="checkbox" onclick="calcular();"class="filled-in" name='chek1' id="chek1" checked="checked" />
                                            <label for="chek1">Formatação e instalação de drivers</label>
                                        </td>
                                        <td class='green-text' id='ck1'>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="checkbox" onclick='calcular();'class="filled-in" name='chek2' id="chek2"/>
                                            <label for="chek2">Limpeza de Disco</label>
                                        </td>
                                        <td class='green-text' id='ck2'>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="checkbox" onclick='calcular();'class="filled-in" name='chek3' id="chek3"/>
                                            <label for="chek3">Otimização do Sistema Operacional</label>
                                        </td>
                                        <td class='green-text' id='ck3'>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="checkbox" onclick="calcular();dropar('qtdbackup');"class="filled-in" name='chek4' id="chek4" checked='checked'/>
                                            <label for="chek4">Backup</label>
                                            <div class="input-field col s12 left-align" id='qtdbackup'>
                                                <input id="valqtdbackup" type="number" min='1' class="validate col s5" value='1' name='valqtdbackup'>
                                                <label for="valqtdbackup" >Tamanho total</label>
                                            </div>
                                        </td>
                                        <td class='green-text' id='ck4'>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="checkbox" onclick='calcular();' class="filled-in"name='chek5' id="chek5"/>
                                            <label for="chek5">Configuração de Firewall(Segurança)</label>
                                        </td>
                                        <td class='green-text' id='ck5'>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="checkbox" onclick="calcular();dropar('qtdprogram');"class="filled-in" name='chek6' id="chek6"/>
                                            <label for="chek6">Instalação de Programas</label>
                                            <div class="input-field col s12 left-align" id='qtdprogram'>
                                                <input id="qtd" type="number" min='1' class="validate col s5" name='qtd'>
                                                <label for="qtd" >Tamanho total</label>
                                            </div>
                                        </td>
                                        <td class='green-text' id='ck6'>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <input type="checkbox" onclick='calcular();'class="filled-in" name='chek7' id="chek7"/>
                                            <label for="chek7">Correção de problemas e remoção de malwares</label>
                                        </td>
                                        <td class='green-text' id='ck7'>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="checkbox" onclick='calcular();'class="filled-in" name='chek8' id="chek8"/>
                                            <label for="chek8">Limpeza Preventiva</label>
                                        </td>
                                        <td class='green-text' id='ck8'>
                                        </td>
                                    </tr>
                                    <tr style='border-top:2px gray solid'>
                                        <td>
                                            Total                            
                                        </td>
                                        <td class='green-text' id='ttl'>
                                        </td>
                                    </tr>
                                    <!--Para adicionar mais opçoes vá em passos.js e adicione um novo preço/valor no vetor "servicos" .. procure por "Calcular preço" -->
                                </tbody>
                            </table>
                        </div>
                        <p class='grey-text text-darken-3'>Modo de Atendimento<span class='red-text'> *</span></p>
                        <p>
                            <input class="" name="resp5" type="radio" id="resp5s" value='visita'  />
                            <label for="resp5s">Visíta Técnica.</label>
                        </p>
                        <p>
                            <input class="" name="resp5" type="radio" id="resp5n"  />
                            <label for="resp5n">Entraga do disposítivo pelo cliente na Empresa.</label>
                        </p>
					    <input type="button" name="next2" class="next acao" value="Proxímo" />
                        <input type="button" name="prev" class="prev acao" value="Voltar" />
                        
                        
                    </fieldset>
                    <fieldset class="col s12 white">
                        <div class="resp  center-align"></div>
                        <h2>Informações de Contato</h2>
                        
                        <div class="row left-align">
                            <div class="input-field col s12 m6 left-align">
                                <i class="material-icons prefix grey-text left-align esquerda text-darken-3 ">account_circle</i>
                                <input id="nome" name='nome' type="text" class="validate">
                                <label for="nome">Nome<span class='red-text'> *</span></label>
                            </div>
                            <div class="input-field col s12 m5 left-align" >
                                <i class="material-icons prefix left-align green-text esquerda text-lighten-1">phone</i>
                                <input id="tel" type="number" name='tel' class="validate">
                                <label for="tel">Telefone/Celular<span class='red-text'> *</span></label>
                            </div>
                            <div class="input-field col s12 m11 left-align" >
                                <i class="material-icons prefix left-align blue-text esquerda ">mail</i>
                                <input id="email" type="email" name='email' class="validate">
                                <label for="email">E-Mail</label>
                            </div>
                        </div>
                        <input style="width:30%;" type="submit" name="Enviar" class="acao Enviar" value="Solicitar" />
                        <input style="width:30%;" type="button" name="prev" class="prev acao correcaoTamanho" value="Voltar" />
					</fieldset>
                </form>
                <div id='posloader' class="col s12 white" style='display:none;margin-top:20%;'>
                    <p class='black-text espacamento text-lighten-2 center-align'><span class='green-text'>Sucesso!</span><br> Entraremos em contato dentro de instantes. Nossa equipe estará sempre pronta a ajuda-lo, agradecemos a preferência!<br>
                        <i class="material-icons green-text" style="font-size: 300%;">done</i>
                    </p>
                    <p class='blue-text center-align'><a class='btn margem' href='index.php'>Página Inícial</a><a href='servicos.php' class='btn margem'>Outros Serviços</a></p>
                </div>
            </div>            
            <script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
            <script type="text/javascript" src="js/materialize.min.js"></script>
            <script type="text/javascript" src="js/jquery.maskedinput.min.js"></script>  
            <script type="text/javascript" src='js/jquery.playSound.js'></script>  
            <script type="text/javascript" src='js/functions.js'></script>
            <script type="text/javascript" src='js/passos.js'></script>
            <script type="text/javascript" src='js/buscacep.js'></script>
            <script>
                //mascaras para campos
                $("#cep").mask("99999-999");
            </script>
           
    </body>
</html>