<!DOCTYPE html>
<!--********************************************************************************************************
    Copyright 2016 Dissotti 
    
    Colaboradores:
    				Marcos Dissotti do Nascimento Rodrigues - marcosdissotti@gmail.com
    				Gabriel Dissotti do Nascimento Rodrigues - gabrieldnrodrigues@gmail.com

    Este arquivo é parte do programa Dissotti

    Dissotti é um software livre; você pode redistribuí-lo e/ou 
    modificá-lo dentro dos termos da Licença Pública Geral GNU como 
    publicada pela Fundação do Software Livre (FSF); na versão 3 da 
    Licença, ou (na sua opinião) qualquer versão.

    Este programa é distribuído na esperança de que possa ser  útil, 
    mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
    a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
    Licença Pública Geral GNU para maiores detalhes.

    Você deve ter recebido uma cópia da Licença Pública Geral GNU junto
    com este programa, Se não, veja <http://www.gnu.org/licenses/>.
    
************************************************************************************************************-->
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
		<meta name="description" content="Somos a solução que cabe no bolso do empreendedor, assim crescemos juntos com o seu negócio!">
		<!-- Sempre força o mais recente mecanismo de renderização do IE (mesmo na intranet) e Chrome Frame Remova isso se você usar o .htaccess-->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>Dissotti</title>
		<meta name="keywords" content="informática, informatica, websites, freelencer, redes, computadores, sistemas, automação, comercial, midia, educação, inovação, ciência">
		<meta name="author" content="Dissotti Desenvolvimento de Software">
		<meta name="google-site-verification" content="adicionar código de autenticação do Google" />
		<meta name="rebots" content="Serviços, Artigos, Sobre">
		<!-- define que a escala 100% é a largura da janela de exibição -->
		<meta name="viewport" content="initial-scale=1.0">

        <!-- Estilo Padrão -->
        <link href='css/estilo.css' rel='stylesheet' type='text/css'/>
        <!-- Materialize -->
        <link href='css/materialize.min.css' rel='stylesheet'/>
        <!-- Material Icons -->
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
    </head>
    <body class=' grey lighten-2'>
        <div class='conteudo'> 
            <!-- estrutura do site -->
            <?php
                include_once("header.php");
            ?>
            <div class='corpo' id='corpo'>
                <div class='corpo container'>
                <!-- Corpo -->
                    <section>
                        <div class="row">
                            <div class="card">
                                <div class="card-content responsive-text">
                                    <article>
                                        <h1 class="header">Licença <img src="midia/GPLv3_Logo.png" class='responsive-img right' style="width: 135px; right: 135px" alt='Ícone da licença GPL Versão 3.0'></h1>
                                        <hr>
                                        <span class="negrito">&copy; 2016 Dissotti Software Livre</span>
                                        <br>
                                        <br>
                                        <span class="negrito">
                                            Colaboradores:
                                        </span>
                                        <br>
                                        <ul>
                                            <li>
                                                Marcos Dissotti do Nascimento Rodrigues, <span class="negrito">marcosdissotti@gmail.com</span>
                                            </li>
                                            <li>
                                                Gabriel Dissotti do Nascimento Rodrigues, <span class="negrito"> gabrieldnrodrigues@gmail.com</span>
                                            </li>
                                        </ul>
                                        <br>
                                        <br>
                                        <p>
                                            <span class="negrito">Versão Não-Oficial da Declaração de Permissão de Cópia</span> (<a href="https://www.gnu.org/licenses/gpl-howto.pt-br.html"><span class="sublinhado negrito">Ver Original</span></a>)
                                        </p>
                                        <br>
                                        <p class='identado responsive-text justificado'>
                                            A Plataforma <span class="negrito">Dissotti</span> é um software livre; você pode 
                                            redistribuí-lo e/ou modificá-lo dentro dos termos da <a href="https://www.gnu.org/licenses/gpl-3.0.html"><span class="sublinhado negrito">Licença Pública Geral GNU</span></a> 
                                            como publicada pela Fundação do Software Livre (FSF); na versão 3 da Licença, 
                                            ou (na sua opinião) qualquer versão.
                                        </p>
                                        <br>
                                        <p class='identado responsive-text justificado'>
                                            Este programa é distribuído na esperança de que possa ser útil, mas <span class="negrito">SEM NENHUMA 
                                            GARANTIA</span>; sem uma garantia implícita de <span class="negrito">ADEQUAÇÃO</span> a qualquer <span class="negrito">MERCADO</span> 
                                            ou <span class="negrito">APLICAÇÃO EM PARTICULAR</span>. Veja a Licença Pública Geral GNU para maiores detalhes.
                                        </p>
                                        <br>
                                        </p>
                                        <p class='identado responsive-text justificado'>
                                            Para ver a Licença Pública Geral GNU deste programa entre no link <a href="http://www.gnu.org/licenses/"><span class="sublinhado negrito">http://www.gnu.org/licenses/</span></a>
                                            ou <a href="https://www.gnu.org/licenses/gpl-3.0.html"><span class="sublinhado negrito">https://www.gnu.org/licenses/gpl-3.0.html</span></a>.
                                        </p>
                                    </div>
                                    <div class="card-action">
                                        <a href="http://www.gnu.org/licenses/"><span class="sublinhado negrito">Ver licença</span></a>
                                    </div>
                                </article>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <?php
                include_once("footer.php");
            ?>
    </body>
</html>