<!DOCTYPE html>
<!--********************************************************************************************************
    Copyright 2016 Dissotti 
    
    Colaboradores:
    				Marcos Dissotti do Nascimento Rodrigues - marcosdissotti@gmail.com
    				Gabriel Dissotti do Nascimento Rodrigues - gabrieldnrodrigues@gmail.com

    Este arquivo é parte do programa Dissotti

    Dissotti é um software livre; você pode redistribuí-lo e/ou 
    modificá-lo dentro dos termos da Licença Pública Geral GNU como 
    publicada pela Fundação do Software Livre (FSF); na versão 3 da 
    Licença, ou (na sua opinião) qualquer versão.

    Este programa é distribuído na esperança de que possa ser  útil, 
    mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
    a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
    Licença Pública Geral GNU para maiores detalhes.

    Você deve ter recebido uma cópia da Licença Pública Geral GNU junto
    com este programa, Se não, veja <http://www.gnu.org/licenses/>.
    
************************************************************************************************************-->
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
		<meta name="description" content="Somos a solução que cabe no bolso do empreendedor, assim crescemos juntos com o seu negócio!">
		<!-- Sempre força o mais recente mecanismo de renderização do IE (mesmo na intranet) e Chrome Frame Remova isso se você usar o .htaccess-->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>Dissotti</title>
		<meta name="keywords" content="informática, informatica, websites, freelencer, redes, computadores, sistemas, automação, comercial, midia, educação, inovação, ciência">
		<meta name="author" content="Dissotti Desenvolvimento de Software">
		<meta name="google-site-verification" content="adicionar código de autenticação do Google" />
		<meta name="rebots" content="Serviços, Artigos, Sobre">
		<!-- define que a escala 100% é a largura da janela de exibição -->
		<meta name="viewport" content="initial-scale=1.0">

        <!-- Estilo Padrão -->
        <link href='css/estilo.css' rel='stylesheet' type='text/css'/>
        <!-- Materialize -->
        <link href='css/materialize.min.css' rel='stylesheet'/>
        <!-- Material Icons -->
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
    </head>
    <body class=' grey lighten-2'>
        <div class='conteudo'> 
            <!-- estrutura do site -->
            <?php
                include_once("header.php");
            ?>
            <div class='corpo' id='corpo'>
            <!-- Corpo -->
                <div>
                    <h1 class='center-align grey-text hide text-darken-1'>Notícias</h1>
                    <div class="row responsive-text">
                        <section class=''> 
                            <h2 class='left-align grey-text text-darken-1 titulo_coluna_noticia'>Repositórios</h2>
                            <!-- ARTIGO -->
                            <article class=" col s12 m6 l6">
                                <div class='card small'>
                                <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator" src="midia/dissotti.png">
                                </div>
                                <div class="card-content">
                                <span class="card-title activator blue-grey-text text-darken-2" target='_blank'>dissotti<i class="material-icons right">more_vert</i></span>
                                <p><a href="https://github.com/MarcosDissotti/dissotti" target='_blank'>Visitar repositório</a></p>
                                </div>
                                <div class="card-reveal">
                                <span class="card-title purple-text text-lighten-1">dissotti<i class="material-icons right">close</i></span>
                                <p>Essa é a aplicação presente sendo desenvolvida.</p>
                                </div>
                                </div>
                            </article>
                            <!-- ARTIGO -->
                            <article class=" col s12 m6 l6">
                                <div class='card small'>
                                <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator" src="midia/wolomgv.png">
                                </div>
                                <div class="card-content">
                                <span class="card-title activator blue-grey-text text-darken-2">wolo-vendas<i class="material-icons right">more_vert</i></span>
                                <p><a href="https://github.com/gabrieldissotti/wolo-vendas" target='_blank' >Visitar repositório</a></p>
                                </div>
                                <div class="card-reveal">
                                <span class="card-title purple-text text-lighten-1 ">wolo-vendas<i class="material-icons right">close</i></span>
                                <p>Este é um módulo para gerenciamento de vendas do sistema Wolo destinada a donos de negócios e pequenos comerciantes.</p>
                                </div>
                                </div>
                            </article>
                            <!-- ARTIGO -->
                            <article class=" col s12 m6 l6">
                                <div class='card small'>
                                <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator" src="midia/planejadosdissotti.png">
                                </div>
                                <div class="card-content">
                                <span class="card-title activator blue-grey-text text-darken-2">planejadosdissotti<i class="material-icons right">more_vert</i></span>
                                <p><a href="https://github.com/gabrieldissotti/planejadosdissotti" target='_blank'>Visitar repositório</a></p>
                                </div>
                                <div class="card-reveal">
                                <span class="card-title purple-text text-lighten-1 ">planejadosdissotti<i class="material-icons right">close</i></span>
                                <p>Este é o website desenvolvido para atender impulsionar o marketing on-line da empresa Planejados Dissotti</p>
                                </div>
                                </div>
                            </article>
                            <!-- ARTIGO -->
                        </section>
                    </div>

                </div>   
            </div>
            <?php
                include_once("footer.php");
            ?>
    </body>
</html>