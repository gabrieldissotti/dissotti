//PASSAR FORMULARIOS    FORM-STEPS

$(function(){
	var atual_fs, next_fs, prev_fs;
	var formulario = $('form[name=cliente_frm]');
	
	function next(elem){
		atual_fs = $(elem).parent();
		next_fs = $(elem).parent().next();


		$('#progress li').eq($('fieldset').index(next_fs)).addClass('ativo');
		atual_fs.fadeOut(300).hide();
		next_fs.fadeIn(400);
	}


	$('.prev').click(function(){
		atual_fs = $(this).parent();
		prev_fs = $(this).parent().prev();
		$('.resp').html('');

		$('#progress li').eq($('fieldset').index(atual_fs)).removeClass('ativo');
		atual_fs.fadeOut(300).hide();
		prev_fs.fadeIn(400);
	});
	
	$('input[name=next1]').click(function(){
		
		var array = formulario.serializeArray();
		if(array[0].value == '' || array[1].value == '' || array[3].value == '' || array[3].value == '...' || array[4].value == '' || array[4].value == '...'){
			erromsg();
			
		}else{
			$('.resp').html('');
			next($(this));
		}
		
	});
	$('input[name=next2]').click(function(){
		var array = formulario.serializeArray();
		
		if($("#test1").is(":checked") == false && $("#test2").is(":checked") == false || ($("#resp5s").is(":checked") == false && $("#resp5n").is(":checked") == false)){//verifica se os checkbox radio estão selecionados
			erromsg();
			
		}
		else if($("#test1").is(":checked") == true && 
		(document.cliente_frm.descricao.value == '' || document.cliente_frm.descricao.value.length < 10
			|| ($("#resp1s").is(":checked")== false && $("#resp1n").is(":checked")== false ) 
			|| ($("#resp2s").is(":checked")== false && $("#resp2n").is(":checked")== false )
			|| ($("#resp3s").is(":checked")== false && $("#resp3n").is(":checked")== false ) 
			|| ($("#resp4s").is(":checked")== false && $("#resp4n").is(":checked")== false ) 
		)
		){
			
			erromsg();
		}
		else if($("#test2").is(":checked") == true && 
			(
				$("#chek1").is(":checked") == false &&
				$("#chek2").is(":checked") == false &&
				$("#chek3").is(":checked") == false &&
				$("#chek4").is(":checked") == false &&
				$("#chek5").is(":checked") == false &&
				$("#chek6").is(":checked") == false &&
				$("#chek7").is(":checked") == false &&
				$("#chek8").is(":checked") == false
			)
		){
			erromsg();
		}
		else{
			$('.resp').html('');
			next($(this));
		}
		
	});	
	$('input[type=submit]').click(function(evento){
		var array = formulario.serializeArray();
		if(
			document.cliente_frm.nome.value.length < 2  ||
			document.cliente_frm.tel.value.length < 6
		 ){	
			
			erromsg();
		}else{
			$.ajax({
			type: 'post',
			url: '../controle/manutencao.php',
			data: {cadastrar: 'sim', campos: array},
			dataType: 'json',
			beforeSend: function(){
				loadingmsg();
			},
			success: function(valor){
				if(valor.erro == 'sim'){
					erromsg(valor.getErro);
				}else{
					okmsg();
					$('#cliente_frm').fadeToggle( "slow", "linear" ).ready(function(){
						$('#posloader').fadeToggle( "slow", "linear" );
						$.playSound('midia/ok'); 
					});					
				}
			},
			error: function( jqXHR, textStatus, errorThrown ){
					
					$('.resp').html('<div class="erros"><p>XHR Status: '+jqXHR.responseText+'</p><p>textStatus: ' + textStatus + '</p><p>errorThrown: ' + errorThrown +'</p></div>');
					
			}
			
		});
		}
		evento.preventDefault();
	});	
});



$(window).resize(function() {
    Responsivo();
});
jQuery(function($) {
    Responsivo();
});
$(document).ready(function() {
    Materialize.updateTextFields();
});

        
//************************* */


/*/*************Calcular Preço*************/
//O Preço é definido substituindo os valores desse vetor!!!   Pode ser float ex 30.23 !!
preco =10; //preco do programa gb
precoBackup =10; //preco do backup gb
servicos = [
	30, // chek1
	10, // chek2
	25, // ...
	precoBackup , //
	100, //
	preco, // programas
	20, // chek7
	15
];
function calcular(){	
	//vamos exibilos no html após finalizar a esta função, para não repetir a cada clique
	
	//define que o total é no maxímo ser 2015 
	var total = 200+servicos[5]+servicos[3] ;//servicos[5] é o valor da quantidade de programas
	//ajusta o passo para 0 para o while
	step=0;
	while(step<8){ //step é a QUANTIDADE DE SERVIÇOS!!!
		//define que a cada passo a vaar chek vai ter o valor de "#chek1" .. "#chek2"
		var chek = "#chek" + (step+1);
		//lembrando que a função é chamada ao clique no RADIO/linha
		//a cada passo, se o RADIO recebe e agora está CHECKED é executado a soma do total pelo serviço adicionado
		if( $(chek).is(":checked") == true){
			total += servicos[(step)];
		}
		//e aqui é executado a subtração, quando o RADIO não Está checado
		else{
			total -= servicos[(step)];
		}
		//aki substituimos o conteudo da tag de id ttl pelo total/2, bem.. não toque nisso se você preza pela sua vida.
		//formata antes de exibir
		var valor = (total/2);
		vlr = mascaraValor(valor.toFixed(2));
		vlr = "R$ " + vlr;
		$("#ttl").html(vlr);
		//verificamos todos os outros campos para validar se o que está selecionado será cobrado
		step++
	}
}
// EXIBE OS VALORES DOS SERVIÇOS DEFINIDOS NA FUNÇÃO ACIMA, no iniciar o documento
$(document).ready(function(){
	var step = 0;
	while(step<8){ //step é a QUANTIDADE DE SERVIÇOS!!!
		var ck = "#ck" + (step+1);
		//formatar e exibir
		valor = servicos[step];
		vlr = mascaraValor(valor.toFixed(2));
		vlr = "R$ " + vlr;
		switch (step) {
			case 5:
				vlr += "/GB"
				$(ck).html(vlr);
				break;
			case 3:
				vlr += "/GB"
				$(ck).html(vlr);
				break;
		
			default:
				$(ck).html(vlr);
				break;
		}
		step++;
		
	}
});


//EXECUTAR AO ALTERAR VALOR DE QUANTIDADE DE PROGRAMAS
$('#qtd').on('change keyup paste click',function(){
  qty = $(this).val();
  servicos[5] = preco*qty;
  //calcula o resultado para aparecer no total do formulário
  calcular();
  //$('meu-botao').attr('href','/busca/quantidade='+qty);
});
//EXECUTAR AO ALTERAR VALOR DE QUANTIDADE DE Backup
$('#valqtdbackup').on('change keyup paste click',function(){
  qty2 = $(this).val();
  servicos[3] = precoBackup*qty2;
  //calcula o resultado para aparecer no total do formulário
  calcular();
  //$('meu-botao').attr('href','/busca/quantidade='+qty);
});


//preloader pre loader
$(document).ready(function(){
    sleep(3000);
    $('#preloader').fadeToggle( "slow", "linear", function(){
    	$('#cliente_frm').fadeToggle( "slow", "linear" ); 
    });
});


//sleep

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}