/************************************************************************************************ 
   
    Copyright 2016 Dissotti 
    
   
    Colaboradores:
    				Marcos Dissotti do Nascimento Rodrigues - marcosdissotti@gmail.com
    				Gabriel Dissotti do Nascimento Rodrigues - gabrieldnrodrigues@gmail.com

   
    Este arquivo é parte do programa Dissotti

    Dissotti é um software livre; você pode redistribuí-lo e/ou 
    modificá-lo dentro dos termos da Licença Pública Geral GNU como 
    publicada pela Fundação do Software Livre (FSF); na versão 3 da 
    Licença, ou (na sua opinião) qualquer versão.

    Este programa é distribuído na esperança de que possa ser  útil, 
    mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
    a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
    Licença Pública Geral GNU para maiores detalhes.

    Você deve ter recebido uma cópia da Licença Pública Geral GNU junto
    com este programa, Se não, veja <http://www.gnu.org/licenses/>.

**************************************************************************************************/

/*********************** UTIL **********************/
function new_sideNav(width_sidNav = 0, edge_sidNav = 'left', close_onclick_sideNav = false, draggable_sideNav = false){
    /*
    Essa função é usada para iniciar um sideNav com os parâmentros informados
    Para utilizar basta chamar a função dentro da página que deseja
    Parâmetros:
    new_sideNav(largura, posição, fechar Ao Clicar, deslize para acionar)
    */

    $('.button-collapse').sideNav({
        menuWidth: width_sidNav, 
        edge: edge_sidNav,
        closeOnClick: close_onclick_sideNav, 
        draggable: draggable_sideNav 
    });
    //$('.collapsible').collapsible(); //uncomment the line below if you use the dropdown variation
    
}

function new_slider(discount_from = 'not', height_slider = 0, indicators_slider = false){
    /*  
        Essa função é usada para iniciar um Slider com os parâmentros informados
        Para utilizar basta chamar a função dentro da página que deseja
        Parâmetros:

        new_slider(se ouver desconto, de que tag{id ou classe da tag} senão, use 'not',altura do slider[0 para 100%], indicadores?{boleano});

    */

    var
    height_window = 0;

    height_window = $(window).height();


    //Quanto terá de desconto na altura.
    if(discount_from != 'not'){
        discont = $(discount_from).height();
    }
    else{
        discont = 0;
    }
    //Qual a altura do slider.
    if(height_slider == 0){
        height_slider = height_window - discont;
    }
    else{
        //Mantém o height_slider do parâmetro
    }
    $('.slider').slider({
        height: height_slider,
        indicators: indicators_slider
    });  
}








  //*************************************************************      





//retira menus do header a caso a tela seja menor que o previsto
//ajusta responsividade das coisas
function Responsivo(){
    //recupero o tamanho da janela de exibição
    var scre = $(window).width();
    if ( scre > 374) {
        $('#inicio').removeClass('hide');
        $('#projetos').addClass('hide');
        $('#servicos').addClass('hide');
        $('#loja').addClass('hide');
    } 
    if ( scre > 620) {
        $('#projetos').removeClass('hide');
        $('#servicos').removeClass('hide');
        $('#projetos').addClass('hide');     
        $('#loja').addClass('hide');            
    } 
    if ( scre > 818) {
        $('#inicio').removeClass('hide');
        $('#projetos').removeClass('hide');
        $('#servicos').removeClass('hide');
    }    
    if (scre > 1000){

        $('#loja').removeClass('hide');
    }
}

// *****************************************


// função para o a troca do conteúdo da página
function goPage(pagina){
    //troca o conteudo da var campo pelo da var página
    $("#corpo").load(pagina).ready(function($){
        Responsivo();
        ajustaExibição('.conteudo','.rodape');//ao carregar ajusta o rodapé
    });
}

//************************************************ */

/*
Torna o rodapé absolute para ficar no bottom em casos de
a altura do conteudo sem menor que a altura do dispositivo
para manter a elegância
*/

function ajustaExibição(conteudo, altera){ //conteudo tem que ser como ex '.conteudo' ou #
    var scre = $(window).height();
    var elemento = $(conteudo).height();
    if(elemento < scre){
        $(altera).css("position", 'absolute');
    }
    else{
        $(altera).css("position", 'relative');
    }
}

//*************************************************/
//Executa algumas funções
//ao carregar a página
jQuery(function() {
    Responsivo();
    ajustaExibição('.conteudo','.rodape');
});

//e aqui ao redimensionar página
$(window).resize(function() {
    Responsivo();
    ajustaExibição('.conteudo','.rodape');
});


//************************** */


//desativar todos os campos filled ins.. (check box)

function desativafiled(identificador){
    $(identificador).hide();
}
function ativafiled(identificador){
    $(identificador).show();
}


//exibir e esconder tags
//ISSO ESTA SENDO USADO NO PASSOS.JS
vsvn = 0;
function dropar(identificador){
    var estado = document.getElementById(identificador).style.display;
    if(estado == 'none'){
        document.getElementById(identificador).style.display = 'block';
    }
    else{
        document.getElementById(identificador).style.display = 'none';
    }
}
//definir como escondido no inicio
$(document).ready(function(){
        $("#qtdprogram").css("display","none");
        //$("#qtdbackup").css("display","none");// atualmente ativado por padrao
        $("#op1").hide();
        //inicializar material box // se nao inicializar nada funciona
        
});


//para formatar dinheiro

function mascaraValor(valor) {
    valor = valor.toString().replace(/\D/g,"");
    valor = valor.toString().replace(/(\d)(\d{8})$/,"$1.$2");
    valor = valor.toString().replace(/(\d)(\d{5})$/,"$1.$2");
    valor = valor.toString().replace(/(\d)(\d{2})$/,"$1,$2");
    return valor                    
}

//exibe mensagem de erro
function erromsg(msg = 'Todos os campos com * devem ser preenchidos devidamente.'){
		$( 'html, body' ).animate( { scrollTop: 0 }, 'slow' );
        $.playSound('midia/erro');
		$('.resp').fadeOut(200);
		$('.resp').fadeIn(300);
		$('.resp').fadeOut(200);
		$('.resp').fadeIn(400);
        saida = '<div class="erros red lighten-1 col s12"><i class="material-icons white-text medium center-align col s12 m2">error_outline</i><p class="white-text col s12 m10">';
        saida += msg;
        saida += ' </p></div>';
		$('.resp').html(saida);
		navigator.vibrate(500);
	}
//exibe mensagem de loading
function loadingmsg(msg = 'Validando seus dados, aguarde um instante...'){
		$( 'html, body' ).animate( { scrollTop: 0 }, 'slow' ); 
		$('.resp').fadeIn(300);
        saida = '<div class="erros green lighten-2 col s12"><i class="material-icons white-text piscar medium center-align col s12 m2">query_builder</i><p class="white-text col s12 m10">';
        saida += msg;
        saida += ' </p></div>';
		$('.resp').html(saida);
		$('.piscar').fadeIn(700);
		$('.piscar').fadeOut(700);
		$('.piscar').fadeIn(700);
		navigator.vibrate(500);
	}
//exibe mensagem de ok
function okmsg(msg = 'Solicitação enviada com sucesso'){
		$( 'html, body' ).animate( { scrollTop: 0 }, 'slow' ); 
		$('.resp').fadeIn(300);
        saida = '<div class="erros green lighten-1 col s12"><i class="material-icons white-text medium center-align col s12 m2">check</i><p class="white-text col s12 m10">';
        saida += msg;
        saida += ' </p></div>';
		$('.resp').html(saida);
		navigator.vibrate(500);
	}


$('.dropdown-button').dropdown({
      inDuration: 300,
      outDuration: 225,
      constrainWidth: false, // Does not change width of dropdown to that of the activator
      hover: true, // Activate on hover
      gutter: 0, // Spacing from edge
      belowOrigin: false, // Displays dropdown below the button
      alignment: 'left', // Displays dropdown with edge aligned to the left of button
      stopPropagation: false // Stops event propagation
    }
  );