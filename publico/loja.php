<!DOCTYPE html>
<!--********************************************************************************************************
    Copyright 2016 Dissotti 
    
    Colaboradores:
    				Marcos Dissotti do Nascimento Rodrigues - marcosdissotti@gmail.com
    				Gabriel Dissotti do Nascimento Rodrigues - gabrieldnrodrigues@gmail.com

    Este arquivo é parte do programa Dissotti

    Dissotti é um software livre; você pode redistribuí-lo e/ou 
    modificá-lo dentro dos termos da Licença Pública Geral GNU como 
    publicada pela Fundação do Software Livre (FSF); na versão 3 da 
    Licença, ou (na sua opinião) qualquer versão.

    Este programa é distribuído na esperança de que possa ser  útil, 
    mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
    a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
    Licença Pública Geral GNU para maiores detalhes.

    Você deve ter recebido uma cópia da Licença Pública Geral GNU junto
    com este programa, Se não, veja <http://www.gnu.org/licenses/>.
    
************************************************************************************************************-->
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
		<meta name="description" content="Somos a solução que cabe no bolso do empreendedor, assim crescemos juntos com o seu negócio!">
		<!-- Sempre força o mais recente mecanismo de renderização do IE (mesmo na intranet) e Chrome Frame Remova isso se você usar o .htaccess-->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>Dissotti</title>
		<meta name="keywords" content="informática, informatica, websites, freelencer, redes, computadores, sistemas, automação, comercial, midia, educação, inovação, ciência">
		<meta name="author" content="Dissotti Desenvolvimento de Software">
		<meta name="google-site-verification" content="adicionar código de autenticação do Google" />
		<meta name="rebots" content="Serviços, Artigos, Sobre">
		<!-- define que a escala 100% é a largura da janela de exibição -->
		<meta name="viewport" content="initial-scale=1.0">

        <!-- Estilo Padrão -->
        <link href='css/estilo.css' rel='stylesheet' type='text/css'/>
        <!-- Materialize -->
        <link href='css/materialize.min.css' rel='stylesheet'/>
        <!-- Material Icons -->
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
    
        
    </head>
    <body class=' grey lighten-2'>
        <div class='conteudo indigo blue lighten-5'> 
            <!-- estrutura do site -->
            <?php
                include_once("header.php");
            ?>
            <div class='corpo ' id='corpo'>
                <!-- coluna de banner e produtos -->
                
                <div class="slider col s12 m8">
                    <ul class="slides">
                    <li>
                        <img src="midia/banners/folhas.jpg"> <!-- random image -->
                        <div class="caption center-align">
                        <h3>Qualidade e Melhor Preço da Região</h3>
                        <h5 class="light grey-text text-lighten-3">Só aqui você encontra o melhor custo-benefício</h5>
                        </div>
                    </li>
                    <li>
                        <img src="midia/banners/vista.jpg"> <!-- random image -->
                        <div class="caption right-align">
                        <h3 class=''>Maior Durabilidade</h3>
                        <h5 class="light grey-text text-lighten-2">Elimine as chances de passar estresse com o que funciona</h5>
                        </div>
                    </li>
                    <li>
                        <img src="midia/banners/flor.jpg"> <!-- random image -->
                        <div class="caption right-align">
                        <h3>Muito Mais Conforto!</h3>
                        <h5 class="light blue-text text-lighten-2"> Ótimas opções para melhorar seu bem-estar</h5>
                        </div>
                    </li>
                    <li>
                        <img src="midia/banners/gato.jpg"> <!-- random image -->
                        <div class="caption center-align">
                        <h3>Faça Negócio Com Quem Entende!</h3>
                        <h5 class="light green-text text-lighten-4">Produtos aprovados em testes e pesquisas.</h5>
                        </div>
                    </li>
                    </ul>
                </div>
                <div class='corpo '>
                
                    <div class="row responsive-text">
                        <br>
                        <!-- menus -->
                        <div class='col s12 m3 amber lighten-5'>
                            <span class="badge left" data-badge-caption="Todos"></span>
                            <span class="new badge blue" data-badge-caption="">1</span><br>
                            <span class="badge left" data-badge-caption="Classificados"></span>
                            <span class="new badge purple lighten-2" data-badge-caption="">0</span><br>
                            <span class="badge left" data-badge-caption="Mouses"></span>
                            <span class="new badge green" data-badge-caption="Novo">1</span><br>
                            <span class="badge left" data-badge-caption="Teclados"></span>
                            <span class="new badge blue" data-badge-caption="Novo">0</span><br>
                            <span class="badge left" data-badge-caption="Pen drive's"></span>
                            <span class="new badge grey" data-badge-caption="Novo">0</span><br>

                        </div>
                        <div class=" col s12 m9 right">
                            <div class="col s12 m6">
                                <div class="card horizontal">
                                    <div class="card-image">
                                        <img class="materialboxed"  src="midia/mouse2.jpg">
                                        <img class="materialboxed"  src="midia/mouse4.jpg">
                                    </div>
                                    <div class="card-stacked">
                                        <div class="card-content">
                                        <p>Mouse Óptico Sem Fio<br>2.4GHz<br>DPI: 3200<br>
                                        <span class='red-text' style='font-size:10pt; text-decoration:line-through;'>De:R$35,00</span><br>
                                        <span class='green-text'>Por:R$25,00</span>
                                        <a href="#">Especificações</a>
                                        </p>
                                        </div>
                                        <div class="card-action">
                                            <a class="waves-effect waves-light btn-large green">Comprar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col s12 m6">
                                <div class="card horizontal">
                                    <div class="card-image">
                                        <img class="materialboxed"  src="midia/mouse.jpg">
                                        <img class="materialboxed"  src="midia/mouse1.jpg">
                                    </div>
                                    <div class="card-stacked">
                                        <div class="card-content">
                                        <p>Mouse Óptico Sem Fio<br>2.4GHz<br>DPI: 3200<br>
                                        <span class='red-text' style='font-size:10pt;text-decoration:line-through;'>De:R$35,00</span><br>
                                        <span class='green-text'>Por:R$25,00</span>
                                        <a href="#">Especificações</a>
                                        
                                        </p>
                                        </div>
                                        <div class="card-action">
                                            <a class="waves-effect waves-light btn-large green">Comprar</a>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            <?php
                include_once("footer.php");
            ?>

            <script>
            new_slider('not',300,false);
            </script>
    </body>
</html>