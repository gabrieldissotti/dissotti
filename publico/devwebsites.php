<!DOCTYPE html>
<!--********************************************************************************************************
    Copyright 2016 Dissotti 
    
    Colaboradores:
    				Marcos Dissotti do Nascimento Rodrigues - marcosdissotti@gmail.com
    				Gabriel Dissotti do Nascimento Rodrigues - gabrieldnrodrigues@gmail.com

    Este arquivo é parte do programa Dissotti

    Dissotti é um software livre; você pode redistribuí-lo e/ou 
    modificá-lo dentro dos termos da Licença Pública Geral GNU como 
    publicada pela Fundação do Software Livre (FSF); na versão 3 da 
    Licença, ou (na sua opinião) qualquer versão.

    Este programa é distribuído na esperança de que possa ser  útil, 
    mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
    a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
    Licença Pública Geral GNU para maiores detalhes.

    Você deve ter recebido uma cópia da Licença Pública Geral GNU junto
    com este programa, Se não, veja <http://www.gnu.org/licenses/>.
    
************************************************************************************************************-->
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
		<meta name="description" content="Somos a solução que cabe no bolso do empreendedor, assim crescemos juntos com o seu negócio!">
		<!-- Sempre força o mais recente mecanismo de renderização do IE (mesmo na intranet) e Chrome Frame Remova isso se você usar o .htaccess-->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>Dissotti</title>
		<meta name="keywords" content="informática, informatica, websites, freelencer, redes, computadores, sistemas, automação, comercial, midia, educação, inovação, ciência">
		<meta name="author" content="Dissotti Desenvolvimento de Software">
		<meta name="google-site-verification" content="adicionar código de autenticação do Google" />
		<meta name="rebots" content="Serviços, Artigos, Sobre">
		<!-- define que a escala 100% é a largura da janela de exibição -->
		<meta name="viewport" content="initial-scale=1.0">

        <!-- Estilo Padrão -->
        <link href='css/estilo.css' rel='stylesheet' type='text/css'/>
        <!-- Materialize -->
        <link href='css/materialize.min.css' rel='stylesheet'/>
        <!-- Material Icons -->
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
    </head>
    <body class=' grey lighten-2'>
        <div class='conteudo'> 
            <!-- estrutura do site -->
            <?php
                include_once("header.php");
            ?>
            <div class='corpo' id='corpo'>
                <div class='corpo'>
                    <div class="row responsive-text">
                        <div class="col s12 m6">
                            <h2 class="header grey-text center-align text-darken-1">Portifólio</h2>
                            <div class="card horizontal">
                            <div class="card-image">
                                <img src="http://lorempixel.com/100/190/nature/6">
                            </div>
                            <div class="card-stacked">
                                <div class="card-content">
                                <p>Confíra os websites vendidos para outras empresas.</p>
                                </div>
                                <div class="card-action">
                                <a href="#" class='sublinhado'>Abrir portifólio</a>
                                </div>
                            </div>
                            </div>
                        </div>

                        <div class="col s12 m6">
                            <h2 class="header grey-text text-darken-1 center-align">Quero o meu...</h2>
                            <div class="card horizontal">
                            <div class="card-image">
                                <img src="http://lorempixel.com/100/190/nature/6">
                            </div>
                            <div class="card-stacked">
                                <div class="card-content">
                                <p>Para solicitar esse serviço você deve começar entendendo o que é necessário e será feito para adquirir seu website.</p>
                                </div>
                                <div class="card-action">
                                <a href="#" class='sublinhado'>Começar...</a>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
                include_once("footer.php");
            ?>
            <script type="text/javascript" src="https://platform.linkedin.com/badges/js/profile.js" async defer></script>
    </body>
</html>