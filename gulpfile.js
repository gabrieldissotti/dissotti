var gulp      = require('gulp'),
    sass      = require('gulp-ruby-sass'),
    prefix    = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    lr = require('tiny-lr'),
    livereload = require('gulp-livereload'),
    server = lr();

gulp.task('compileStyles', function() {
    gulp.src('gabriel/dating/sass/*.{sass,scss}')
    .pipe(
        sass('gabriel/dating/sass/*.{sass,scss}')
		.on('error', sass.logError)
        .pipe(prefix('last 3 version'))
        .pipe(minifycss())
        .pipe(gulp.dest('gabriel/dating/css/'))
        .pipe(livereload(server))
    );
});



gulp.task('watch', function() {
    server.listen(35729, function( err ) {
        if ( err ) { return console.log( err ); }
        gulp.watch('gabriel/dating/**/*.{sass,scss,htm}', ['compileStyles']);
    });
});

gulp.task('default', ['watch']);
