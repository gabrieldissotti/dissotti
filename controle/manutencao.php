<?php

    //Dependencias - requere antes de iniciar o programa
    require_once("valida.php");
    require_once("classes/manutencao.php");
    require_once("util.php");
    require_once("classes/cliente.php");
    require_once("classes/avaliacao.php");
    require_once("classes/pedido.php");
    require_once("classes/Connection.php");
    //VALIDA E DEPOIS EXECUTA AS AFUNÇÕES DE CADASTRAR O PEDIDO DE MANUTENÇÃO
    if(isset($_POST['cadastrar']) && $_POST['cadastrar'] == 'sim'){
        //VALIDA DADOS ENVIADOS PELA CAMADA DE APRESENTACAO
        $novos_campos = array();
        $campos_post = $_POST['campos'];
        $respostas = array();
        //CONVERTE DO TIPO JSON PARA PHP ARRAY
        foreach ($campos_post as $indice => $valor) {
            $novos_campos[$valor['name']] = $valor['value'];
        }
        if(validaCep($novos_campos['cep'])){
            $respostas['erro'] = 'sim';
            $respostas['getErro']  = 'CEP é inválido';
        }
        elseif(!validaLogradouro($novos_campos['logradouro'])){
            $respostas['erro'] = 'sim';
            $respostas['getErro']  = 'Logradouro é inválido';
        }
        /*elseif(!validaComplemento($novos_campos['complemento'])){
            $respostas['erro'] = 'sim';
            $respostas['getErro']  .= ' - Complemento é inválido';
        }*/
        elseif(!validaBairro($novos_campos['bairro'])){
            $respostas['erro'] = 'sim';
            $respostas['getErro']  = 'Bairro é inválido';
        }
        elseif(!validaDescricao($novos_campos['descricao'])){
            $respostas['erro'] = 'sim';
            $respostas['getErro']  = 'Descrição é inválida';
        }
        elseif(!validaRua($novos_campos['rua'])){
            $respostas['erro'] = 'sim';
            $respostas['getErro']  = 'Rua é inválida';
        }
        elseif(!validaNome($novos_campos['nome'])){
            $respostas['erro'] = 'sim';
            $respostas['getErro']  = 'Nome é inválido';
        }
        elseif(!validaTelCel($novos_campos['tel'])){
            $respostas['erro'] = 'sim';
            $respostas['getErro']  = 'Telefone/Celular é inválido';
        }
        elseif(!validaEmail($novos_campos['email'])){
            $respostas['erro'] = 'sim';
            $respostas['getErro']  = 'Email é inválido';
        }
        else{
                //DEFININDO ATRIBUTOS DO CLIENTE
                $objCliente = new Cliente();

                //preparando valores
                $cep = trim($novos_campos['cep']);
                $cep = toNum($cep);
                $logradouro = trim($novos_campos['logradouro']);
                $telcel = toNum($novos_campos['tel']);


                $objCliente->cep = $cep; 
                $objCliente->logradouro = $logradouro;
                $objCliente->complemento = $novos_campos['complemento'];
                $objCliente->rua = $novos_campos['rua'];
                $objCliente->bairro = $novos_campos['bairro'];
                $objCliente->nome = $novos_campos['nome'];
                $objCliente->email = $novos_campos['email'];
                $objCliente->telcel = $telcel;
                //se a primeira opção é selecionada
                if($novos_campos['group1'] == '1'){
                    //DEFININDO ATRIBUTOS DA AVALIAÇÃO
                    $objAvaliacao = new Avaliacao();
                    if($novos_campos['resp1'] == 'sim'){
                        $objAvaliacao->resp1 = 1;
                    }
                    if($novos_campos['resp2'] == 'sim'){
                        $objAvaliacao->resp2 = 1;
                    }
                    if($novos_campos['resp3'] == 'sim'){
                        $objAvaliacao->resp3 = 1;
                    }
                    if($novos_campos['resp4'] == 'sim'){
                        $objAvaliacao->resp4 = 1;
                    }
                    $objAvaliacao->descricao = $novos_campos['descricao'];
                    }
                elseif($novos_campos['group1'] == '2'){
                    //DEFININDO ATRIBUTOS DA MANUTENCAO
                    $objManutencao = new Manutencao();
                    $preco = 0;
                    if(isset($novos_campos['chek1'])){
                        $objManutencao->formatacao = 1;
                        $preco += 30;
                    }
                    if(isset($novos_campos['chek2'])){
                        $objManutencao->limpezaDisco = 1;
                        $preco += 10;
                    }
                    if(isset($novos_campos['chek3'])){
                        $objManutencao->otimizacaoSO = 1;
                        $preco += 25;
                    }
                    if(isset($novos_campos['chek4']) && validaQtd($novos_campos['valqtdbackup']) == true){
                        $objManutencao->backup = $novos_campos['valqtdbackup'];
                        $preco += (10*$novos_campos['valqtdbackup']);
                    }
                    if(isset($novos_campos['chek5'])){
                        $objManutencao->firewall = 1;
                        $preco += (100);
                    }
                    if(isset($novos_campos['chek6']) && validaQtd($novos_campos['qtd'])){
                        $objManutencao->programas = $novos_campos['qtd'];
                        $preco += (10*$novos_campos['qtd']);
                    }
                    if(isset($novos_campos['chek7'])){
                        $objManutencao->problemasMalwares = 1;
                        $preco += (20);
                    }
                    if(isset($novos_campos['chek8'])){
                        $objManutencao->preventiva = 1;
                        $preco += (15);
                    }
                    
                    $objManutencao->preco = $preco;
                }

                //DEFININDO ATRIBUTOS DO PEDIDO
                $objPedido = new Pedido();
                
                $objPedido->tipo = 0; // 0 é manutencao
                if($novos_campos['resp5'] == 'visita'){
                    $objPedido->atendimento = 'v'; // v é visita
                    $objPedido->tipo = 'manutencao';
                }

                //hora de executar as funções de cada classe
                
                //ao executar isto temos o id cliente definido
                $objCliente->cadastrarCliente();
                // cadastramos o pedido
                $objPedido->cadastrarPedido($objCliente->idCliente);
                if($novos_campos['group1'] == '1'){
                    $objAvaliacao->cadastrarAvaliacao();//criar
                }
                elseif($novos_campos['group1'] == '2'){
                    $objManutencao->cadastrarManutencao();
                }
                //descomente para hablitar envio de e-mail como notificação de novo pedido de manutenção
                //include('send-msg.php');
                $respostas['erro'] = 'nao';
                $respostas['msg']  = 'Sucesso!';    
            }
            echo json_encode($respostas);
        }        
    
?>