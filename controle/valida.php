<?php
    //Dependencias
    require_once("util.php");

    //VALIDAR CEP
    function validaCep($cep) {
        //ESTA FUNCAO NAO GARANTE QUE O CEP EXISTA NA BASE DE DADOS DO CORREIOS
        //torna cep apenas numeros
        $cep = trim($cep);
        $cep = toNum($cep);
        if(count($cep) < 8 || count($cep) > 8){
            return false;
        }
        else{
            return true;
        }
    }
    //VALIDAR LOGRADOURO
    function validaLogradouro($logradouro){
        if(!preg_match("/^([a-z-0-9 ]+)$/i", $logradouro)){
            return false;
        }
        else{
            return true;
        }
    }
    //VALIDAR COMPLEMENTO
    function validaComplemento($complemento){
        if(!preg_match("/^([a-z-0-9 ]+)$/i", $complemento)){
            return false;
        }
        else{
            return true;
        }
    }
    //VALIDAR RUA
    function validaRua($rua){
        return true;
    }
    //VALIDAR BAIRRO
    function validaBairro($bairro){
        return true;
    }
    //VALIDAR DESCRICAO
    function validaDescricao($descricao){
        if(strlen($descricao) > 501 || strlen($descricao) < 0){
            return false;
        }
        else{
            return true;
        }
    }
    //VALIDAR NOME
    function validaNome($nome){
        if(!preg_match("/^([a-z ]+)$/i", $nome)){
            return false;
        }
        else{
            return true;
        }
    }
    //VALIDAR EMAIL
    function validaEmail($email){
        $er = "/^(([0-9a-zA-Z]+[-._+&])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}){0,1}$/";
        if(isset($email) && $email != '' && !preg_match($er, $email)){
            return false;
        }
        else{
            return true;
        }
    }

    //VALIDAR QUANTIDADE
    function validaQtd($qtd){
        if($qtd < 1){
            return false;
        }
        else{
            return true;
        }
    }
    //VALIDAR TELEFONE E CELULAR
    function validaTelCel($telcel){
        return true; //valido
    }
?>