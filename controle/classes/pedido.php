<?php
    class Pedido{
        private $idPedido;
		private $tipo;
        private $atendimento; // empresa ou visita


		function __construct(){
			$objConnection = new Connection();
		    $objConnection->Conectar();
	   	}

        //metodos mágicos
        public function __set($atrib, $value){
            $this->$atrib = $value;
        }

        public function __get($atrib){
            return $this->$atrib; 
        }


        public function cadastrarPedido($idCliente){
            $data = array(
			'tipo' 		        => $this->tipo,
            'Cliente_idCliente' => $idCliente,
			'atendimento'		=> $this->atendimento
			);
			$objConnection = new Connection();
		    $objConnection->Conectar();
			$objConnection->DBCreate('Pedido', $data);
        }
        
    }

?>