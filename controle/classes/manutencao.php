<?php
    class Manutencao{
        private $idManutencao;
		private $formatacao;
        private $limpezaDisco;
        private $otimizacaoSO;
        private $backup;
        private $firewall;
        private $programas;
        private $problemasMalwares;
        private $preventiva;
        private $preco;


		function __construct(){
			$objConnection = new Connection();
		    $objConnection->Conectar();
	   	}

        //metodos mágicos
        public function __set($atrib, $value){
            $this->$atrib = $value;
        } 
        public function __get($atrib){
            return $this->$atrib; 
        }

        public function cadastrarManutencao(){
            $data = array(
			'formatacao' => $this->formatacao,
            'limpezaDisco' => $this->limpezaDisco,
			'otimizacaoSO' => $this->otimizacaoSO,
            'backup' => $this->backup,
            'firewall' => $this->firewall,
            'programas' => $this->programas,
            'preco' => $this->preco,
            'preventiva' => $this->preventiva,
            'problemasMalwares' => $this->problemasMalwares
			);
			$objConnection = new Connection();
		    $objConnection->Conectar();
			$objConnection->DBCreate('Manutencao', $data);
        }
        
    }

?>