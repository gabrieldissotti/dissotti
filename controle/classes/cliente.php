<?php
    class Cliente{
        private $idCliente;
		private $email;
		private $nome;
		private $rua;
		private $bairro;
		private $telcel;
		private $cep;
		private $complemento;
		private $logradouro;


		function __construct(){
			$objConnection = new Connection();
		    $objConnection->Conectar();
	   	}

		//metodos mágicos
        public function __set($atrib, $value){
            $this->$atrib = $value;
        }
		 
        public function __get($atrib){
            return $this->$atrib;
        }

		public function cadastrarCliente(){
			$data = array(
			'email'     => $this->email,
            //'Cliente_idCliente' => $this->Cliente_idCliente,
			'nome'		=> $this->nome,
			'rua' 	=> $this->rua,
			'bairro' 	=> $this->bairro,
			'telcel' 	=> $this->telcel,
			'cep' 	=> $this->cep,
			'complemento' 	=> $this->complemento,
			'logradouro' 	=> $this->logradouro
			);
			$objConnection = new Connection();
		    $objConnection->Conectar();
			//cadastra o cliente no banco e retorna o id do registro!!
			//por isso deve ser executado antes do pedido, que vai utilizar tal idate
			//porque o cliente não é cadastrado antes, ainda.
			//melhoraremos isso
			$this->idCliente = $objConnection->DBCreate('Cliente', $data, true);
		}
    }

?>