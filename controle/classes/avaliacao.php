<?php
    class Avaliacao{
        private $idAvaliacao;
		private $resp1;
        private $resp2;
        private $resp3;
        private $resp4;
        private $descricao;

		function __construct(){
			$objConnection = new Connection();
		    $objConnection->Conectar();
	   	}

        //metodos mágicos
        public function __set($atrib, $value){
            $this->$atrib = $value;
        } 
        public function __get($atrib){
            return $this->$atrib; 
        }
        

        public function cadastrarAvaliacao(){
            $data = array(
			'descricao' => $this->descricao,
            'resp1' => $this->resp1,
			'resp2' => $this->resp2,
            'resp3' => $this->resp3,
            'resp4' => $this->resp4,
			);
			$objConnection = new Connection();
		    $objConnection->Conectar();
			$objConnection->DBCreate('Avaliacao', $data);
        }
    }

?>