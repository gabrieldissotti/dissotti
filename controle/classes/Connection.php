<?php
	class Connection{
	   var $Server = "localhost";
	   var $Username = "root";
	   var $Password = "";
	   var $Database = "dissotti";
	   var $Porta = "3306";
	   var $charset = "utf8";
	 
	 
	 /*
	  * 
	  *  
	  * 
	  * 
	  * 
	  * */
	   function __construct(){
	      $this->Conectar();
		  
		//	ini_set( 'display_errors', true );
		//	error_reporting( E_ALL );
	   }
	 //ABRE CONEXÃO com MySQL(DB
	   function Conectar(){
          if($_SERVER['SERVER_ADDR']  == "127.0.0.1" ){
              $link = mysqli_connect($this->Server,$this->Username,$this->Password,$this->Database) or die(mysqli_connect_error());
              mysqli_set_charset($link, $this->charset) or die(mysqli_error($link));
          }
          else{
              $this->Server = "mysql.hostinger.com.br";
              $this->Username = "u528391500_root";
              $this->Password = "@lun0$2016";
              $this->Database = "u528391500_sgmp";
              $this->Porta = "3306";
              $this->charset = "utf8";
              $link = mysqli_connect($this->Server,$this->Username,$this->Password,$this->Database)or die(mysqli_connect_error());
              mysqli_set_charset($link, $this->charset) or die(mysqli_error($link));
          }
          return $link;
	   }   
   
   //FECHA CONEXÃO com MySQL

   function DBClose($link){
      @mysqli_close($link) or die(mysqli_error($link));
   }
    /* FUNÇÕES DE CONSULTAS NO DATABASE*/
   /***********************************/

   //DELETA REGISTROS
   
   function DBDelete($table, $where = null){
      $table  = $table;
      $where  = ($where) ? " WHERE {$where}" : null;
      
      $query  = "DELETE FROM {$table}{$where}";

      return DBExecute($query);
   }

   /************************************************************************************************/

   //ALTERA REGISTROS

   function DBUpDate($table, array $data, $where = null, $insertId = false){
      foreach($data as $key => $value){
         $fields[] = "{$key} = '{$value}'";
      }
      $fields = implode(', ', $fields);

      $table  = $table;
      $where  = ($where) ? " WHERE {$where}" : null;

      $query  = "UPDATE {$table} SET {$fields}{$where}";
      return $this->DBExecute($query, $insertId);
   }

   /*************************************************************************************************/

   //LER REGISTROS
   
   function DBRead($table, $params = null, $fields = '*'){
      $table  =  $table;
      $params = ($params) ? " {$params}" : null;
      
      $query  = "SELECT {$fields} FROM {$table}{$params}";
      $result = $this->DBExecute($query);
      
      if(!mysqli_num_rows($result)){
         return false;
      }
      else{
         while($res = mysqli_fetch_assoc($result)){
            $data[] = $res;
			
         }
	  	 return $data;
      }
   }

   /**************************************************************************************************/

   //GRAVA REGISTROS

   function DBCreate($table, array $data, $insertId = false){
      $table  =  $table;
      $data   =  $this->DBEscape($data);
      $fields = implode(", ", array_keys($data));
      $values = "'".implode("', '", $data)."'";
      
      $query  =  "INSERT INTO {$table}( {$fields} ) VALUES( {$values} )";

      return $this->DBExecute($query, $insertId);
   }

   /**************************************************************************************************/

   // EXECUTA QUERYS(Consultas)

   function DBExecute($query, $insertId = false){
      $link   = $this->Conectar(); //Inicia Conexão
      $result = @mysqli_query($link, $query) or die(mysqli_error($link));

      if($insertId){
         $result = mysqli_insert_id($link);
      }

      $this->DBClose($link); //Fecha Conexão
      return $result; // retorna valor da função para $query
   }
   
   
   
   //CONTRA SQL INJECTION
   
   
   function DBEscape($dados){
      $link = $this->Conectar();
      if(!is_array($dados)){
         $dados = mysqli_real_escape_string($link, $dados);
      }
      else{
         $arr = $dados;
         foreach($arr as $key => $value){
            $key      = mysqli_real_escape_string($link, $key);
            $value    = mysqli_real_escape_string($link, $value);
            $dados[$key] = $value;
         }
      }
      $this->DBClose($link);
      return $dados;
   }
}
?>