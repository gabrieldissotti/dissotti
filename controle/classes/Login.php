<?php
	require_once('Connection.php');
	session_start();
	
	Class Login{
	 
	   function __construct(){
	      $objConnection = new Connection();
	   }
	 
	   function verificarLogado(){
	      if(!isset($_SESSION["logado"])){
	         header("Location:login.php");
			
	      }
		  if (isset($_SESSION["sessiontime"])){ 
                if ($_SESSION["sessiontime"] < time() ) 
                {
                    session_destroy();
                    header ("location:login.php");
                   #se session for menor que o time ele
                   #destroi a session e redireciona pra login
                } 
                else
                {
                   echo "<span style='display:none;' id='sessiontime'> " . ($_SESSION["sessiontime"] - time()) . "</span>"; 

                }
            } 
            else
            {
               header ("location:login.php");
                #se sessiontime tiver vazia ele ja direto pra login.php
            }
	   }
		function renovar(){
			  $_SESSION["sessiontime"] = time() + 1800;
                   #se session for maior que o o time ele adiciona mais 360
                   #na sessiontime 
		}

		 
	   function Logar($email,$senha){
	       $objConnection = new Connection();
		   $objConnection->Conectar();
		  
		   $usuario = $objConnection->DBRead("Administrador","where Administrador.email ='{$email}'","*");
	      if(isset($usuario)){
	         if($usuario[0]["senha"] == $senha){
	         	
	            $_SESSION["id_usuario"] = $usuario[0]["idAdministrador"];
	            $_SESSION["logado"] = "sim";
				$_SESSION["sessiontime"] = (time() + 1800); //tempo de sessão em segundo
	            header("Location:home.php");
	         }else{
	            $Erro = "<span><img src='midia/megaphone-1.png' /></span><p>Senha e/ou Email inválido(s)!<br> Tente novamente.</p>";
	            return $Erro;
	         }
	      }else{
	         $Erro = "<span><img src='midia/megaphone-1.png' /></span><p>Senha e/ou Email inválido(s)!<br> Tente novamente.</p>";
	         return $Erro;
	      };
	   }
	 
	   function getIdUsuario(){
	      return $_SESSION["id_usuario"];
	   }
	 
	   function deslogar(){
	      session_destroy();
	      header("Location: login.php");
	   }
	 
	}
?>